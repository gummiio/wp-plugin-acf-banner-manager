acf.add_action('change', ($el) => {
    let $field = $el.closest('.acf-field');
    let warningMessage = $field.data('acf-banner_manager-warning');

    if (! warningMessage) {
        return;
    }

    if ($el.is(':checked')) {
        $field.find('.acf-switch').append(`<p class="acf-banner_manager-warning">${warningMessage}</p>`);
    } else {
        $field.find('.acf-banner_manager-warning').remove();
    }
});

acf.add_action('ready', ($el) => {
    jQuery('[data-acf-banner_manager-warning] .acf-switch-input', $el).trigger('change');
});
