<?php
/*
    Plugin Name: Advanced Custom Fields: Banner Manager
    Plugin URI: https://acf-banner-manager.gummi.io/
    Description: A banner manager that extends/utilizes on advanced custom fields plugin.
    Version: 1.0.0
    Author: Alan Chen
    License: GPLv2 or later
    License URI: http://www.gnu.org/licenses/gpl-2.0.html
*/

defined('ABSPATH') or die('No script kiddies please!');

require_once dirname(__FILE__) . '/vendor/autoload.php';

global $acfBannerManager;
$acfBannerManager = new \GummiIO\AcfBannerManager\BannerManager(__FILE__, '1.0.0');
