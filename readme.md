# Advanced Custom Fields Module - Banner Manager
Control post types/terms/archives/pages banner globally with dynamic acf fields.

In the frontend, can just call `banner_field('banner_title')` or `get_banner_field('banner_title')`, it'll automatically fallback based on you current page. For example, if you are on a category term archive page, it'll chekc if individual control for category is enabled and a value is set. If not, then it'll check if global control for category is enabled and fallback to the global category setting. Lastly, fallback to the default banner settings if none of above matches.

[ ![Bitbucket Pipeline Status for gummiio/wp-plugin-acf-banner-manager](https://bitbucket-badges.atlassian.io/badge/gummiio/wp-plugin-acf-banner-manager.svg)](https://bitbucket.org/gummiio/wp-plugin-acf-banner-manager/overview)

##### - Example of how options page looks like (just like a regular acf options page)
![Screenshot 1](screenshots/1.jpg)

##### - Each registered post type or taxonomy can be controled whether the banner field group appears in the global setting, or in its edit page
![Screenshot 2](screenshots/2.jpg)

##### - Helpful merge tags for dynamic text based on the current post
![Screenshot 3](screenshots/3.jpg)

##### - It really just a regular field group, so when this field group is edited, it'd apply to all the banner
![Screenshot 4](screenshots/4.jpg)

##### - On the post single, it's just a regular field group
![Screenshot 5](screenshots/5.jpg)
