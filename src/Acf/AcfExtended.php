<?php

namespace GummiIO\AcfBannerManager\Acf;

class AcfExtended
{
    public function __construct()
    {
        add_filter('acf/prepare_field/type=select', [$this, 'addFieldGroupsChoices']);
        add_filter('acf/prepare_field/type=select', [$this, 'addPostTypesChoices']);
        add_filter('acf/prepare_field/type=select', [$this, 'addTaxonomiesChoices']);
        add_filter('acf/prepare_field/type=select', [$this, 'addPageTemplatesChoices']);
        add_filter('acf/prepare_field/type=select', [$this, 'addBannerFieldGroupChoices']);
        add_filter('acf/prepare_field/type=text',   [$this, 'addPrimaryTitleInstructions']);
        add_filter('acf/prepare_field',             [$this, 'addMergeTagsToolTip']);
        add_filter('acf/prepare_field',             [$this, 'markdownFieldInstructions']);
        add_filter('acf/get_field_groups',          [$this, 'excludeSettingOptionGroup'], 25);
        add_filter('acf/get_field_groups',          [$this, 'resetBannerFieldGroupLocations']);
        add_action('display_post_states',           [$this, 'addBannerGroupIndicator'], 10, 2);
    }

    public function addFieldGroupsChoices($field)
    {
        if (! data_get($field, 'acf_banner_manager_field_groups')) {
            return $field;
        }

        $choices = collect(acf_get_field_groups())
            ->where('key', '!=', acf_banner_manager_option_group('key'))
            ->pluck('title', 'key')
            ->all();

        $field['choices'] = apply_filters('acf/banner_manager/field_groups_field_choices', $choices);

        return $field;
    }

    public function addPostTypesChoices($field)
    {
        if (! data_get($field, 'acf_banner_manager_post_types')) {
            return $field;
        }

        $choices = collect(get_post_types(['public' => true], 'objects'))
            ->pluck('label', 'name')
            ->all();

        $field['choices'] = apply_filters('acf/banner_manager/post_types_field_choices', $choices);

        return $field;
    }

    public function addTaxonomiesChoices($field)
    {
        if (! data_get($field, 'acf_banner_manager_taxonomies')) {
            return $field;
        }

        $choices = collect(get_taxonomies(['public' => true], 'objects'))
            ->pluck('label', 'name')
            ->all();

        $field['choices'] = apply_filters('acf/banner_manager/taxonomies_field_choices', $choices);

        return $field;
    }

    public function addPageTemplatesChoices($field)
    {
        if (! data_get($field, 'acf_banner_manager_page_templates')) {
            return $field;
        }

        $choices = array_merge([
            'default' => apply_filters('default_page_template_title',  __('Default Template', 'acf'))
        ], wp_get_theme()->get_page_templates());

        $field['choices'] = apply_filters('acf/banner_manager/page_templates_field_choices', $choices);

        return $field;
    }

    public function addBannerFieldGroupChoices($field)
    {
        if (! $fieldTypes = data_get($field, 'acf_banner_manager_banner_group_fields')) {
            return $field;
        }

        $choices = collect(acf_get_fields(acf_banner_manager_banner_group('key')))
            ->whereIn('type', is_array($fieldTypes)? $fieldTypes : ['text'])
            ->pluck('label', 'key')
            ->all();

        $field['choices'] = apply_filters('acf/banner_manager/primary_title_field_choices', $choices);

        return $field;
    }

    public function addPrimaryTitleInstructions($field)
    {
        if (! $fieldKey = acf_banner_manager_get_value('primary_title_field')) {
            return $field;
        }

        if ($fieldKey != data_get($field, 'key')) {
            return $field;
        }

        $field['instructions'] = implode(' ', array_filter([
            $field['instructions'],
            __('(Leave blank to use current object name)', 'acf-banner_manager')
        ]));

        return $field;
    }

    public function addMergeTagsToolTip($field)
    {
        if (! acf_banner_manager_get_value('enable_merge_tags')) {
            return $field;
        }

        if (! $this->isMergeTagField($field)) {
            return $field;
        }

        $tooltip = collect(acf_banner_manager_merge_tags())
            ->map(function($mergeTag, $tag) {
                return sprintf('<b>%s</b>: <i>%s</i>', $tag, data_get($mergeTag, 'description'));
            })
            ->prepend(sprintf('<b>%s</b>', __('Merge Tags:', 'acf-banner_manager')))
            ->implode('<br/>');

        $field['label'] .= sprintf(
            ' <span class="acf-js-tooltip acf-banner_manager-tooltip" title="%s">
                <span class="dashicons dashicons-filter"></span>
            </span>',
            esc_attr($tooltip)
        );

        return $field;
    }

    public function markdownFieldInstructions($field)
    {
        if (acf_maybe_get_GET('page') != acf_banner_manager_option_page('menu_slug')) {
            return $field;
        }

        if (data_get($field, 'instructions')) {
            $field['instructions'] = preg_replace('/\*\*([^\*]+)\*\*/i', '<b>$1</b>', $field['instructions']);
        }

        if (data_get($field, 'message')) {
            $field['message'] = preg_replace('/\*\*([^\*]+)\*\*/i', '<b>$1</b>', $field['message']);
        }

        if (data_get($field, 'instructions')) {
            $field['instructions'] = preg_replace('/`([^`]+)`/i', '<code>$1</code>', $field['instructions']);
        }

        if (data_get($field, 'message')) {
            $field['message'] = preg_replace('/`([^`]+)`/i', '<code>$1</code>', $field['message']);
        }

        return $field;
    }

    public function excludeSettingOptionGroup($fieldGroups)
    {
        if (acf_maybe_get_GET('page') == acf_banner_manager_option_page('menu_slug')) {
            return $fieldGroups;
        }

        return collect($fieldGroups)
            ->where('key', '!=', acf_banner_manager_option_group('key'))
            ->all();
    }

    public function resetBannerFieldGroupLocations($fieldGroups)
    {
        if (acf_maybe_get_GET('page') != acf_banner_manager_option_page('menu_slug')) {
            return $fieldGroups;
        }

        return collect($fieldGroups)
            ->map(function($fieldGroup) {
                if (data_get($fieldGroup, 'key') == acf_banner_manager_banner_group('key')) {
                    $fieldGroup['location'] = [];
                }

                return $fieldGroup;
            })
            ->all();
    }

    public function addBannerGroupIndicator($postStatus, $post)
    {
        if (! acf_is_screen('edit-acf-field-group')) {
            return $postStatus;
        }

        if ($post->ID != acf_banner_manager_banner_group('ID')) {
            return $postStatus;
        }

        $postStatus['acf_banner_group'] = __('Banner Group', 'acf-banner_manager');

        return $postStatus;
    }

    protected function wrapperHasClass($field, $class)
    {
        return strpos(data_get($field, 'wrapper.class'), $class) !== false;
    }

    protected function isMergeTagField($field)
    {
        return !! collect(acf_banner_manager_get_value('banner_merge_tag_fields')? : [])
            ->first(function($tagField) use ($field) {
                return strpos($field['key'], $tagField) !== false;
            });
    }
}
