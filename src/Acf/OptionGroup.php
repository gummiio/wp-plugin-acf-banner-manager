<?php

namespace GummiIO\AcfBannerManager\Acf;

use GummiIO\AcfBannerManager\OptionGroup\PostTypeTab;
use GummiIO\AcfBannerManager\OptionGroup\SettingTab;
use GummiIO\AcfBannerManager\OptionGroup\SpecialTab;
use GummiIO\AcfBannerManager\OptionGroup\TaxonomyTab;

class OptionGroup
{
    protected $prefix = 'acf_banner_manager';
    public $fieldGroup;

    public function __construct()
    {
        // add_action('admin_menu', [$this,'loadOptionPage'], 100); // acf is 99
        add_action('wp_loaded', [$this, 'loadOptionFieldGroup']);
    }

    public function loadOptionPage()
    {
        add_action($this->getOptionPageHook(), [$this, 'loadOptionFieldGroup'], 5); // acf is 10
    }

    public function loadOptionFieldGroup()
    {
        $this->fieldGroup = $this->createOptionLocalGroup();

        $this->loadDefaultTabs();

        do_action('acf/banner_manager/options_field_group/fields', $this, $this->fieldGroup);
    }

    protected function getOptionPageHook()
    {
        global $_registered_pages;

        $hook = collect($_registered_pages)->keys()->first(function($page) {
            return strpos($page, acf_banner_manager_option_page('menu_slug')) !== false;
        });

        return "load-{$hook}";
    }

    protected function createOptionLocalGroup($args = [])
    {
        acf_add_local_field_group($args = wp_parse_args($args, [
            'key'      => acf_banner_manager_field_name('settings', 'group_'),
            'title'    => __('Banner Settings', 'acf-banner_manager'),
            'private'  => true,
            'location' => [
                [
                    [
                        'param'    => 'options_page',
                        'operator' => '==',
                        'value'    => acf_banner_manager_option_page('menu_slug')
                    ],
                ],
            ],
        ]));

        return acf_get_field_group($args['key']);
    }

    protected function loadDefaultTabs()
    {
        $this->settingTab       = new SettingTab;
        $this->specialTab       = new SpecialTab;
        $this->postTypeTab      = new PostTypeTab;
        $this->taxonomyTab      = new TaxonomyTab;
    }

    public function addField($type, $title, $name, $args = [])
    {
        acf_add_local_field($args = wp_parse_args($args, [
            'parent' => data_get($this->fieldGroup, 'key'),
            'key'    => acf_banner_manager_field_name($name, 'field_'),
            'name'   => acf_banner_manager_field_name($name),
            'label'  => $title,
            'type'   => $type,
        ]));

        return acf_get_field($args['key']);
    }

    public function sectionTab($title, $icon, $name = null)
    {
        $this->addField(
            'tab',
            sprintf('<span class="dashicons dashicons-%s"></span> %s', $icon, $title),
            $name? : uniqid('heading_tab-'),
            ['placement' => 'left']
        );
    }

    public function itemTab($title, $args = [])
    {
        $this->addField('tab', $title, uniqid('indent_tab-'), $args);
    }

    public function disabledTab($title, $args = [])
    {
        $this->addField('tab', $title, uniqid('disabled_tab-'), $args);
    }

    public function message($message, $type, $icon)
    {
        $this->addField('message', null, uniqid('message-'), [
            'message' => sprintf(
                '<div class="acf-banner_manager-setting-notice --%s">
                    <span class="dashicons dashicons-%s"></span>
                    <span>%s</span>
                </div>',
                $type, $icon, $message
            ),
            'wrapper' => ['class' => 'acf-banner_manager-no-label']
        ]);
    }

    public function infoMessage($message)
    {
        $this->message($message, 'info', 'smiley');
    }

    public function errorMessage($message)
    {
        $this->message($message, 'error', 'info');
    }

    public function warningMessage($message)
    {
        $this->message($message, 'warn', 'warning');
    }

    public function headingMessage($title, $message = '', $notice = '')
    {
        $notice = $notice? sprintf(
            '<div class="acf-banner_manager-setting-notice --warn">
                <span class="dashicons dashicons-warning"></span>
                <span>%s</span>
            </div>', $notice
        ) : '';

        $this->addField('message', $title, uniqid('message-'), [
            'message' => $message . $notice,
            'wrapper' => ['class' => 'acf-banner_manager-heading']
        ]);
    }

    public function fieldGroupSelect($title, $name, $args = [])
    {
        $this->addField('select', $title, $name, wp_parse_args($args, [
            'ui'           => 1,
            'allow_null'   => 1,
            'acf_banner_manager_field_groups' => 1,
        ]));
    }

    public function postTypesSelect($title, $name, $args = [])
    {
        $this->addField('select', $title, $name, wp_parse_args($args, [
            'ui'           => 1,
            'allow_null'   => 1,
            'multiple'     => 1,
            'acf_banner_manager_post_types' => 1,
        ]));
    }

    public function taxonomiesSelect($title, $name, $args = [])
    {
        $this->addField('select', $title, $name, wp_parse_args($args, [
            'ui'           => 1,
            'allow_null'   => 1,
            'multiple'     => 1,
            'acf_banner_manager_taxonomies' => 1,
        ]));
    }

    public function cloneBannerGroup($name, $args = [])
    {
        if (! acf_banner_manager_banner_group()) {
            $this->errorMessage(__('Please select a field group from settings for the banner first.', 'acf-banner_manager'));

            return;
        }

        $fields = collect(acf_get_fields(acf_banner_manager_banner_group('key')))
            ->where('type', '!=', 'tab')
            ->pluck('key')
            ->all();

        $this->addField('clone', 'adsf', $name, wp_parse_args($args, [
            'clone'       => $fields,
            'prefix_name' => 1
        ]));

        do_action('acf/banner_manager/option_group/after_banner_group', $this, $name, $args);
    }
}
