<?php

namespace GummiIO\AcfBannerManager\Acf;

use GummiIO\AcfBannerManager\Acf\OptionGroup;

class OptionPage
{
    public $optionPage;
    public $optionGroup;

    public function __construct()
    {
        add_action('acf/init', [$this, 'addOptionPage']);
        add_action('acf/init', [$this, 'registerOptionPageFields'], 15);

        add_action('acf/save_post', [$this, 'maybeGenerateDefaultBannerGroup'], 20);
        add_action('acf/save_post', [$this, 'updateBannerGroupLocations'], 20);
    }

    public function getOptionGroup($property = null)
    {
        return $property? data_get($this->optionGroup, $property) : $this->optionGroup;
    }

    public function addOptionPage()
    {
        $args = apply_filters('acf/banner_manager/options_sub_page_args', [
            'page_title' => __('ACF Banner Manager', 'acf-banner_manager'),
            'menu_title' => __('Banner', 'acf-banner_manager')
        ]);

        if (! $page = acf_add_options_sub_page($args)) {
            $page = acf_options_page()->validate_page($args);
        }

        $this->optionPage = acf_get_options_page(data_get($page, 'menu_slug'));
    }

    public function registerOptionPageFields()
    {
        $this->optionGroup = new OptionGroup;
    }

    public function maybeGenerateDefaultBannerGroup($postId)
    {
        if (! $this->isSavingBannerSetting($postId)) {
            return;
        }

        if (! acf_banner_manager_get_value('generate_field_group')) {
            return;
        }

        acf_disable_cache();

        $fieldGroup = $this->generateDefaultBannerGroup();

        $bannerTitle = collect(acf_get_fields(data_get($fieldGroup, 'key')))
            ->first(function($field) {
                return $field['name'] == 'banner_title';
            });

        acf_banner_manager_save_value('banner_field_group', data_get($fieldGroup, 'key'));
        acf_banner_manager_save_value('generate_field_group', false);
        acf_banner_manager_save_value('primary_title_field', data_get($bannerTitle, 'key'));
    }

    public function updateBannerGroupLocations($postId)
    {
        if (! $this->isSavingBannerSetting($postId)) {
            return;
        }

        $this->adjustBannerGroupLocation();
    }

    protected function isSavingBannerSetting($postId)
    {
        if ($postId != 'options') {
            return false;
        }

        return strpos(data_get(get_current_screen(), 'id'), acf_banner_manager_option_page('menu_slug')) !== false;
    }

    protected function generateDefaultBannerGroup()
    {
        $fieldGroup = acf_update_field_group([
            'key'        => uniqid('group_'),
            'title'      => __('Page Banner', 'acf-banner_manager'),
            'menu_order' => 100,
            'position'   => 'acf_after_title',
            'location'   => [],
        ]);

        acf_update_field([
            'parent'       => $fieldGroup['ID'],
            'key'          => uniqid('field_'),
            'label'        => __('Banner Title', 'acf-banner_manager'),
            'name'         => 'banner_title',
            'type'         => 'text',
            'wrapper'      => ['width' => '70', 'class' => 'acf-banner_manager-merge-tags-tooptip']
        ]);

        acf_update_field([
            'parent'      => $fieldGroup['ID'],
            'key'         => uniqid('field_'),
            'label'       => __('Banner Background', 'acf-banner_manager'),
            'name'        => 'banner_background',
            'type'        => 'image',
            'wrapper'     => ['width' => '30']
        ]);

        do_action('acf/banner_manager/default_field_group', $fieldGroup['ID']);

        return $fieldGroup;
    }

    protected function adjustBannerGroupLocation()
    {
        if (! $bannerGroup  = acf_banner_manager_banner_group()) {
            return;
        }

        $newLocations = [];
        $postTypes    = collect(acf_banner_manager_get_value('individual_post_types')? : []);
        $taxonomies   = collect(acf_banner_manager_get_value('individual_taxonomies')? : []);
        $templates    = collect(acf_banner_manager_excluded_templates());

        if ($postTypes->contains('page')) {
            $location = [['param' => 'post_type', 'operator' => '==', 'value' => 'page']];

            if (! $templates->isEmpty()) {
                $templates->each(function($template) use (& $location) {
                    $location[] = ['param' => 'page_template', 'operator' => '!=', 'value' => $template];
                });
            }

            $newLocations[] = $location;
        }

        foreach ($postTypes as $postType) {
            if ($postType == 'page') continue;

            $newLocations[] = [['param' => 'post_type', 'operator' => '==', 'value' => $postType]];
        }

        foreach ($taxonomies as $taxonomy) {
            $newLocations[] = [['param' => 'taxonomy', 'operator' => '==', 'value' => $taxonomy]];
        }

        $newLocations[] = [[
            'param' => 'options_page',
            'operator' => '==',
            'value' => acf_banner_manager_option_page('menu_slug')
        ]];

        $bannerGroup['location'] = apply_filters('acf/banner_manager/banner_group_location', $newLocations);

        acf_update_field_group($bannerGroup);
    }
}
