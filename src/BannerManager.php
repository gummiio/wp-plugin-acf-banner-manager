<?php

namespace GummiIO\AcfBannerManager;

use GummiIO\AcfBannerManager\Acf\AcfExtended;
use GummiIO\AcfBannerManager\Acf\OptionPage;
use GummiIO\AcfBannerManager\Core\Admin;
use GummiIO\AcfBannerManager\Core\Updater;
use GummiIO\AcfBannerManager\Output\MergeTag;
use GummiIO\AcfBannerManager\Output\PrimaryTitle;
use GummiIO\AcfBannerManager\Output\Shortcode;

class BannerManager
{
    protected $file;
    protected $version;

    public function __construct($file, $version)
    {
        $this->file = $file;
        $this->version = $version;

        add_action('acf/init', [$this, 'importDefaultOptions']);
        add_action('plugins_loaded', [$this, 'initialize']);
        add_action('plugins_loaded', [$this, 'loadLanguages']);
        add_action('admin_notices', [$this, 'printAcfRequiredNotice']);
    }

    public function version()
    {
        return $this->version;
    }

    public function file()
    {
        return $this->file;
    }

    public function path($path = '')
    {
        return untrailingslashit(plugin_dir_path($this->file) . untrailingslashit($path));
    }

    public function url($uri = '')
    {
        return untrailingslashit(plugin_dir_url($this->file) . untrailingslashit($uri));
    }

    public function initialize()
    {
        $this->updater      = new Updater;
        $this->admin        = new Admin;
        $this->acfExtended  = new AcfExtended;
        $this->optionPage   = new OptionPage;
        $this->mergeTag     = new MergeTag;
        $this->primaryTitle = new PrimaryTitle;
        $this->shortcode    = new Shortcode;

        $this->loadBuiltInAddons();

        do_action('acf/banner_manager/init', $this);

        return $this;
    }

    public function importDefaultOptions()
    {
        if (acf_banner_manager_get_value('individual_post_types') === null) {
            acf_banner_manager_save_value('individual_post_types', ['page']);
        }

        if (acf_banner_manager_get_value('enable_merge_tags') === null) {
            acf_banner_manager_save_value('enable_merge_tags', true);
        }
    }

    public function loadLanguages()
    {
        load_plugin_textdomain('acf-banner_manager', false, $this->path('lang'));
    }

    public function printAcfRequiredNotice()
    {
        if (class_exists('acf')) {
            return;
        }

        printf('
            <div class="notice notice-error">
                <p>%s</p>
            </div>',
            sprintf(
                __('%s requires the plugin %s to be activated.', 'acf-banner_manager'),
                '<b>' . __('ACF Banner Manager', 'acf-banner_manager') . '</b>',
                '<b>' . __('Advanced Custom Fields Pro', 'acf-banner_manager') . '</b>'
            )
        );
    }

    public function loadBuiltInAddons()
    {
        //
    }
}
