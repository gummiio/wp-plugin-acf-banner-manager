<?php

namespace GummiIO\AcfBannerManager\Core;

class Admin
{
    public function __construct()
    {
        add_action('admin_enqueue_scripts', [$this, 'enqueyeSripts']);
    }

    public function enqueyeSripts()
    {
        wp_enqueue_style(
            'acf-banner-manager',
            acf_banner_manager()->url('assets/css/acf-banner-manager.css'),
            [],
            filemtime(acf_banner_manager()->path('assets/css/acf-banner-manager.css'))
        );

        wp_enqueue_script(
            'acf-banner-manager',
            acf_banner_manager()->url('assets/js/acf-banner-manager.min.js'),
            ['acf-pro-input'],
            filemtime(acf_banner_manager()->path('assets/js/acf-banner-manager.min.js'))
        );
    }
}
