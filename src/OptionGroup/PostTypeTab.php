<?php

namespace GummiIO\AcfBannerManager\OptionGroup;

class PostTypeTab
{
    public function __construct()
    {
        add_action('acf/banner_manager/options_field_group/fields', [$this, 'populatePostTypeFields'], 30, 2);

        add_action('acf/banner_manager/options_field_group/fields/post_type_tab', [$this, 'populateSettings'], 10);
        add_action('acf/banner_manager/options_field_group/fields/post_type_tab', [$this, 'populatePostTypes'], 20);
    }

    public function populatePostTypeFields($factory, $fieldGroup)
    {
        $factory->sectionTab(__('Post Types', 'acf-banner_manager'), 'media-default');

        do_action('acf/banner_manager/options_field_group/fields/post_type_tab', $factory, $fieldGroup);
    }

    public function populateSettings($factory)
    {
        $messages = [
            __("Enabling **global control** of a post type will allow its banner's value to be set globally. Global post type's values are checked and used right before it uses the default. ", 'acf-banner_manager'),
            __("Enabling **individual control** of a post type will allow a post type banner's value to be set individually on its admin edit page. Individual post type's values are checked and used first before any other fallbacks. ", 'acf-banner_manager'),
        ];

        $factory->headingMessage(
            __('Post Type Settings:', 'acf-banner_manager'),
            implode("\n\n", $messages),
            __('Settings will be applied after update.', 'acf-banner_manager')
        );

        $factory->postTypesSelect(__('Global Control Post Types', 'acf-banner_manager'), 'global_post_types', [
            'instructions' => __('Select the post types to have global control.', 'acf-banner_manager'),
        ]);

        $factory->postTypesSelect(__('Individual Control Post Types', 'acf-banner_manager'), 'individual_post_types', [
            'instructions' => __('Select the post types to have individual controlon its edit page.', 'acf-banner_manager'),
        ]);

        $factory->addField('select', __('Excluded Page Templates', 'acf-banner_manager'), 'excluded_templates', [
            'instructions' => __('Exclude these page templates for individual control.', 'acf-banner_manager'),
            'ui'           => 1,
            'allow_null'   => 1,
            'multiple'     => 1,
            'conditional_logic' => [
                [[
                    'field' => acf_banner_manager_field_name('individual_post_types', 'field_'),
                    'operator' => '==',
                    'value' => 'page',
                ]]
            ],
            'acf_banner_manager_page_templates' => 1
        ]);
    }

    public function populatePostTypes($factory)
    {
        if (! $postTypes = acf_banner_manager_global_post_types()) {
            $factory->disabledTab(__('(not available)', 'acf-banner_manager'));
            return;
        }

        collect($postTypes)->each(function($postType) use ($factory) {
            if (! $postType = get_post_type_object($postType)) {
                return;
            }

            $factory->itemTab($postType->label);

            $factory->headingMessage(
                sprintf(__('Global Banner for: %s', 'acf-banner_manager'), $postType->label),
                sprintf(__('This is where the %s banner values are set globally.', 'acf-banner_manager'), $postType->label)
            );

            $factory->cloneBannerGroup("post_type_{$postType->name}");
        });
    }
}
