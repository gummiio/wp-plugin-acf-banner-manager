<?php

namespace GummiIO\AcfBannerManager\OptionGroup;

class SettingTab
{
    public function __construct()
    {
        add_action('acf/banner_manager/options_field_group/fields', [$this, 'populateSettingFields'], 10, 2);
    }

    public function populateSettingFields($factory, $fieldGroup)
    {
        $factory->sectionTab(__('Settings', 'acf-banner_manager'), 'admin-settings', 'settings_tab');

        $factory->headingMessage(
            __('Banner Manager Settings:', 'acf-banner_manager'),
            '', // __("", 'acf-banner_manager'),
            __('Settings will be applied after update.', 'acf-banner_manager')
        );

        $factory->fieldGroupSelect(__('Banner Field Group', 'acf-banner_manager'), 'banner_field_group', [
            'instructions' => __('Select a field group to be used for the banner. (Tabs are not recommanded in the field group)', 'acf-banner_manager'),
        ]);

        $factory->addField('true_false', __('Re-import Default Field Group', 'acf-banner_manager'), 'generate_field_group', [
            'instructions' => __("Check this if you don't already have a banner field group setup and want to import the default banner fields and use it as banner field group.", 'acf-banner_manager'),
            'ui'           => 1,
            'wrapper'      => ['class' => 'acf-banner_manager-inline-true_false']
        ]);

        $factory->addField('true_false', __('Allow Shortcodes', 'acf-banner_manager'), 'allow_shortcodes', [
            'instructions' => __("Check this if you want to allow shortcodes in the banner's field values.", 'acf-banner_manager'),
            'ui'           => 1,
            'wrapper'      => ['class' => 'acf-banner_manager-inline-true_false']
        ]);

        $factory->addField('true_false', __('Enable Merge Tags', 'acf-banner_manager'), 'enable_merge_tags', [
            'instructions' => __("Check this if you want to enable merge tags. Merge tags are some useful **dynamic string** based on the current query object. Such as, current post title, current term name etc...", 'acf-banner_manager'),
            'ui'           => 1,
            'wrapper'      => ['class' => 'acf-banner_manager-inline-true_false']
        ]);

        if (acf_banner_manager_banner_group()) {
            $factory->addField('select', __('Primary Title Field', 'acf-banner_manager'), 'primary_title_field', [
                'instructions' => __("Select a **text field** from the selected banner field group that holds the banner title. If the primary title field's value is empty. It will automatically populate with current object's title. Such as, current post title, current archive title etc...", 'acf-banner_manager'),
                'ui'           => 1,
                'allow_null'   => 1,
                'acf_banner_manager_banner_group_fields' => 1
            ]);

            $factory->addField('select', __('Merge Tag Fields', 'acf-banner_manager'), 'banner_merge_tag_fields', [
                'instructions' => __("Select a **text field**, **textarea field** or **wysiwyg field** from the selected banner field group that should allow merge tags.", 'acf-banner_manager'),
                'ui'           => 1,
                'allow_null'   => 1,
                'multiple'     => 1,
                'acf_banner_manager_banner_group_fields' => ['text', 'textarea', 'wysiwyg']
            ]);
        }

        do_action('acf/banner_manager/options_field_group/fields/setting_tab', $factory, $fieldGroup);
    }
}
