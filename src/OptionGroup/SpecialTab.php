<?php

namespace GummiIO\AcfBannerManager\OptionGroup;

class SpecialTab
{
    public function __construct()
    {
        add_action('acf/banner_manager/options_field_group/fields', [$this, 'populateSpecialFields'], 20, 2);

        add_action('acf/banner_manager/options_field_group/fields/special_tab', [$this, 'populateSettings'], 10);
        add_action('acf/banner_manager/options_field_group/fields/special_tab', [$this, 'populateDefault'], 20);
        add_action('acf/banner_manager/options_field_group/fields/special_tab', [$this, 'populate404'], 20);
        add_action('acf/banner_manager/options_field_group/fields/special_tab', [$this, 'populateSearch'], 30);
        add_action('acf/banner_manager/options_field_group/fields/special_tab', [$this, 'populateAuthor'], 40);
        add_action('acf/banner_manager/options_field_group/fields/special_tab', [$this, 'populateYear'], 50);
        add_action('acf/banner_manager/options_field_group/fields/special_tab', [$this, 'populateMonth'], 60);
        add_action('acf/banner_manager/options_field_group/fields/special_tab', [$this, 'populateDay'], 70);
    }

    public function populateSpecialFields($factory, $fieldGroup)
    {
        $factory->sectionTab(__('Special Banners', 'acf-banner_manager'), 'star-filled');

        do_action('acf/banner_manager/options_field_group/fields/special_tab', $factory, $fieldGroup);
    }

    public function populateSettings($factory)
    {
        $factory->headingMessage(
            __('Sepcial Banners:', 'acf-banner_manager'),
            __("Some pages in Wordpress, such as 404 or search results that has no edit screen in the backend. If you would like to have control over those pages, this is where those special pages can be enabled. If a special page is enabled, the banner manager will check and use the value that's entered before using the default.", 'acf-banner_manager'),
            __('Settings will be applied after update.', 'acf-banner_manager')
        );

        $factory->addField('checkbox', __('Enabled Special Banner Types:', 'acf-banner_manager'), 'enabled_special_type', [
            'choices'    => apply_filters('acf/banner_manager/options_field_group/fields/special_tab_choices', [
                '404'    => '404',
                'search' => 'Search Results',
                'author' => 'Author Archive',
                'year'   => 'Year Archive',
                'month'  => 'Month Archive',
                'day'    => 'Day Archive',
            ])
        ]);
    }

    public function populateDefault($factory)
    {
        $factory->itemTab(__('Defaults', 'acf-banner_manager'));

        $factory->headingMessage(
            __('Default Banner:', 'acf-banner_manager'),
            __("This is where the default banner values are set. Default banner value is the last fallback that the filter will run if no value is entered in other specific banner fields.", 'acf-banner_manager')
        );

        $factory->cloneBannerGroup('default');
    }

    public function populate404($factory)
    {
        if (! $this->enabled('404')) {
            return;
        }

        $factory->itemTab(__('404 Page', 'acf-banner_manager'), '404');

        $this->headingFunctionMessage(
            $factory,
            __('404 Page Banner:', 'acf-banner_manager'),
            __("This is where the banner values for 404 page are set. 404 pages banner's values are used when wordpress's %s condition returns true.", 'acf-banner_manager'),
            'is_404'
        );

        $factory->cloneBannerGroup('404');
    }

    public function populateSearch($factory)
    {
        if (! $this->enabled('search')) {
            return;
        }

        $factory->itemTab(__('Search Results', 'acf-banner_manager'), 'search');

        $message = [
            __("This is where the banner values for search results page are set. Search results page banner's values are used when wordpress's %s condition returns true.", 'acf-banner_manager'),
            $this->additionalMergeTagsMessage([
                '{{search_query}}' => __('The current search query string.', 'acf-banner_manager')
            ])
        ];

        $this->headingFunctionMessage(
            $factory,
            __('Search Results Page Banner:', 'acf-banner_manager'),
            implode("\n",$message),
            'is_search'
        );

        $factory->cloneBannerGroup('search');
    }

    public function populateAuthor($factory)
    {
        if (! $this->enabled('author')) {
            return;
        }

        $factory->itemTab(__('Author Archive', 'acf-banner_manager'), 'author');

        $message = [
            __("This is where the banner values for author archive page are set. Author archive page banner's values are used when wordpress's %s condition returns true.", 'acf-banner_manager'),
            $this->additionalMergeTagsMessage([
                '{{author_name}}' => __('The author display name for current archive.', 'acf-banner_manager')
            ])
        ];

        $this->headingFunctionMessage(
            $factory,
            __('Author Archive Banner:', 'acf-banner_manager'),
            implode("\n",$message),
            'is_author'
        );

        $factory->cloneBannerGroup('author');
    }

    public function populateYear($factory)
    {
        if (! $this->enabled('year')) {
            return;
        }

        $factory->itemTab(__('Year Archive', 'acf-banner_manager'), 'year');

        $message = [
            __("This is where the banner values for year archive page are set. Year archive page banner's values are used when wordpress's %s condition returns true.", 'acf-banner_manager'),
            $this->additionalMergeTagsMessage([
                '{{year}}' => __('The year for current archive.', 'acf-banner_manager')
            ])
        ];

        $this->headingFunctionMessage(
            $factory,
            __('Year Archive Banner:', 'acf-banner_manager'),
            implode("\n",$message),
            'is_year'
        );

        $factory->cloneBannerGroup('year');
    }

    public function populateMonth($factory)
    {
        if (! $this->enabled('month')) {
            return;
        }

        $factory->itemTab(__('Month Archive', 'acf-banner_manager'), 'month');

        $message = [
            __("This is where the banner values for month archive page are set. Month archive page banner's values are used when wordpress's %s condition returns true.", 'acf-banner_manager'),
            $this->additionalMergeTagsMessage([
                '{{month}}' => __('The month for current archive.', 'acf-banner_manager')
            ])
        ];

        $this->headingFunctionMessage(
            $factory,
            __('Month Archive Banner:', 'acf-banner_manager'),
            implode("\n",$message),
            'is_month'
        );

        $factory->cloneBannerGroup('month');
    }

    public function populateDay($factory)
    {
        if (! $this->enabled('day')) {
            return;
        }

        $factory->itemTab(__('Day Archive', 'acf-banner_manager'), 'day');

        $message = [
            __("This is where the banner values for day archive page are set. Day archive page banner's values are used when wordpress's %s condition returns true.", 'acf-banner_manager'),
            $this->additionalMergeTagsMessage([
                '{{day}}' => __('The day for current archive.', 'acf-banner_manager')
            ])
        ];

        $this->headingFunctionMessage(
            $factory,
            __('Day Archive Banner:', 'acf-banner_manager'),
            implode("\n",$message),
            'is_day'
        );

        $factory->cloneBannerGroup('day');
    }

    protected function enabled($type)
    {
        return in_array($type, acf_banner_manager_get_value('enabled_special_type')? : []);
    }

    protected function headingFunctionMessage($factory, $title, $message, $function)
    {
        $factory->headingMessage($title, sprintf(
            $message,
            sprintf(
                '<a href="%s" target="_blank" rel="noopener noreferrer">%s</a>',
                "https://developer.wordpress.org/reference/functions/${function}/",
                "<code>${function}()</code>"
            )
        ));
    }

    protected function additionalMergeTagsMessage($mergeTags)
    {
        if (! acf_banner_manager_get_value('enable_merge_tags')) {
            return '';
        }

        $message = '<div class="acf-banner_manager-additional-merge-tags">';

        $message .= sprintf('<b>%s</b><br/>', __('There are additional merge tags available for this banner:', 'acf-banner_manager'));

        $message .= '<ul>';
        foreach ($mergeTags as $tag => $description) {
            $message .= sprintf('<li><code>%s</code>: %s</li>', $tag, $description);
        }
        $message .= '</ul>';

        $message .= '</div>';

        return $message;
    }
}
