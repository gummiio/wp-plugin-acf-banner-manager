<?php

namespace GummiIO\AcfBannerManager\OptionGroup;

class TaxonomyTab
{
    public function __construct()
    {
        add_action('acf/banner_manager/options_field_group/fields', [$this, 'populateTaxonomyFields'], 40, 2);

        add_action('acf/banner_manager/options_field_group/fields/taxonomy_tab', [$this, 'populateSettings'], 10);
        add_action('acf/banner_manager/options_field_group/fields/taxonomy_tab', [$this, 'populateTaxonomies'], 20);
    }

    public function populateTaxonomyFields($factory, $fieldGroup)
    {
        $factory->sectionTab(__('Taxonomies', 'acf-banner_manager'), 'networking');

        do_action('acf/banner_manager/options_field_group/fields/taxonomy_tab', $factory, $fieldGroup);
    }

    public function populateSettings($factory)
    {
        $messages = [
            __("Enabling **global control** of a taxonomy will allow its banner's value to be set globally. Global taxonomy's values are checked and used right before it uses the default. ", 'acf-banner_manager'),
            __("Enabling **individual control** of a taxonomy will allow a taxonomy banner's value to be set individually on its admin edit page. Individual taxonomy's values are checked and used first before any other fallbacks. ", 'acf-banner_manager'),
        ];

        $factory->headingMessage(
            __('Taxonomy Settings:', 'acf-banner_manager'),
            implode("\n\n", $messages),
            __('Settings will be applied after update.', 'acf-banner_manager')
        );

        $factory->taxonomiesSelect(__('Global Control Taxonomies', 'acf-banner_manager'), 'global_taxonomies', [
            'instructions' => __('Select the taxonomies to have global control.', 'acf-banner_manager'),
        ]);

        $factory->taxonomiesSelect(__('Individual Control Taxonomies', 'acf-banner_manager'), 'individual_taxonomies', [
            'instructions' => __('Select the taxonomies to have individual control on its edit page.', 'acf-banner_manager'),
        ]);
    }

    public function populateTaxonomies($factory)
    {
        if (! $taxonomies = acf_banner_manager_global_taxonomies()) {
            $factory->disabledTab(__('(not available)', 'acf-banner_manager'));
            return;
        }

        collect($taxonomies)->each(function($taxonomy) use ($factory) {
            if (! $taxonomy = get_taxonomy($taxonomy)) {
                return;
            }

            $factory->itemTab($taxonomy->label);

            $factory->headingMessage(
                sprintf(__('Global Banner for: %s', 'acf-banner_manager'), $taxonomy->label),
                sprintf(__('This is where the %s banner values are set globally.', 'acf-banner_manager'), $taxonomy->label)
            );

            $factory->cloneBannerGroup("taxonomy_{$taxonomy->name}");
        });
    }
}
