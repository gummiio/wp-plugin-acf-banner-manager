<?php

namespace GummiIO\AcfBannerManager\Output;

class MergeTag
{
    public function __construct()
    {
        add_filter('acf/banner_manager/merge_tags', [$this, 'adddefaultMergeTags']);
        add_filter('acf/banner_manager/banner_field_value', [$this, 'replaceMergeTags'], 10, 2);
    }

    public function adddefaultMergeTags($mergeTags)
    {
        return array_merge($mergeTags, $this->defaultMergeTags());
    }

    public function replaceMergeTags($value, $outputter)
    {
        if (! acf_banner_manager_get_value('enable_merge_tags')) {
            return $value;
        }

        if (! $this->isMergeTagField($outputter)) {
            return $value;
        }

        if (! is_string($value)) {
            return $value;
        }

        foreach (acf_banner_manager_merge_tags() as $key => $props) {

            if (strpos($value, $key) === false) {
                continue;
            }

            if (! $function = data_get($props, 'callback')) {
                continue;
            }

            if (is_callable($function)) {
                $result = $function();
                $value = str_replace($key, $result, $value);
                continue;
            }

            if (is_array($function) && is_callable($function[0])) {
                $result = call_user_func_array('call_user_func', $function);
                $value = str_replace($key, $result, $value);
            }
        }

        return $value;
    }

    protected function defaultMergeTags()
    {
        $defaults = [
            '{{page_title}}' => [
                'description' => __('Page Title'),
                'callback' => 'get_the_title'
            ],

            '{{term_name}}' => [
                'description' => __('Term Name'),
                'callback' => function() { return data_get(get_queried_object(), 'name'); }
            ],

            '{{term_description}}' => [
                'description' => __('Term Description'),
                'callback' => function() { return data_get(get_queried_object(), 'description'); }
            ],

            '{{taxonomy_name}}' => [
                'description' => __('Taxnome Name'),
                'callback' => function() {
                    return data_get(get_taxonomy(data_get(get_queried_object(), 'taxonomy')), 'labels.singular_name');
                }
            ],
        ];

        if ($this->hasSpecialType('search')) {
            $defaults['{{search_query}}'] = [
                'description' => __('Search Query'),
                'callback' => 'get_search_query'
            ];
        }

        if ($this->hasSpecialType('author')) {
            $defaults['{{author_name}}'] = [
                'description' => __('Current Author Name'),
                'callback' => 'get_the_author'
            ];
        }

        if ($this->hasSpecialType('year')) {
            $defaults['{{year}}'] = [
                'description' => __('Current Post Year'),
                'callback' => ['get_the_date', 'Y']
            ];
        }

        if ($this->hasSpecialType('year') || $this->hasSpecialType('month')) {
            $defaults['{{month}}'] = [
                'description' => __('Current Post Month'),
                'callback' => ['get_the_date', 'F']
            ];
        }

        if ($this->hasSpecialType('year') || $this->hasSpecialType('month') || $this->hasSpecialType('day')) {
            $defaults['{{day}}'] = [
                'description' => __('Current Post Day'),
                'callback' => ['get_the_date', 'j']
            ];
        }

        return $defaults;
    }

    protected function hasSpecialType($type)
    {
        return in_array($type, acf_banner_manager_get_value('enabled_special_type')? : []);
    }

    protected function isMergeTagField($outputter)
    {
        $field = $outputter->getOriginalField();

        return !! collect(acf_banner_manager_get_value('banner_merge_tag_fields')? : [])
            ->first(function($tagField) use ($field) {
                return strpos($field['key'], $tagField) !== false;
            });
    }
}
