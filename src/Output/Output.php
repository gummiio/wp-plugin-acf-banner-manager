<?php

namespace GummiIO\AcfBannerManager\Output;

class Output
{
    protected $originalFieldName;
    protected $fieldName;
    protected $reference;

    public function __construct($fieldName)
    {
        $this->originalFieldName = $fieldName;

        $this->calculateValue();
    }

    public function getCallableArguments()
    {
        return [$this->getFieldName(), $this->getReference()];
    }

    public function getFieldName()
    {
        return $this->fieldName;
    }

    public function getReference()
    {
        return $this->reference;
    }

    public function getValue()
    {
        return apply_filters(
            'acf/banner_manager/banner_field_value',
            call_user_func_array('get_field', $this->getCallableArguments()),
            $this
        );
    }

    public function getField($property = null)
    {
        $field = acf_get_field($this->fieldName, $this->reference);

        return $property? data_get($field, $property) : $field;
    }

    public function getOriginalField($property = null)
    {
        $field = acf_get_field($this->originalFieldName, 'options');

        return $property? data_get($field, $property) : $field;
    }

    protected function calculateValue()
    {
        $fieldName = $this->originalFieldName;
        $reference = null;

        if ($reference && get_field($fieldName, $reference)) {
            $this->fieldName = $fieldName;
            $this->reference = $reference;
        } else
        if ($this->isSpecial('day') && $this->getSpecialValue('day', $fieldName)) {
            $this->fieldName = acf_banner_manager_field_name("day_{$fieldName}");
            $this->reference = 'options';
        } else
        if ($this->isSpecial('month') && $this->getSpecialValue('month', $fieldName)) {
            $this->fieldName = acf_banner_manager_field_name("month_{$fieldName}");
            $this->reference = 'options';
        } else
        if ($this->isSpecial('year') && $this->getSpecialValue('year', $fieldName)) {
            $this->fieldName = acf_banner_manager_field_name("year_{$fieldName}");
            $this->reference = 'options';
        } else
        if ($this->isSpecial('author') && $this->getSpecialValue('author', $fieldName)) {
            $this->fieldName = acf_banner_manager_field_name("author_{$fieldName}");
            $this->reference = 'options';
        } else
        if ($this->isSpecial('search') && $this->getSpecialValue('search', $fieldName)) {
            $this->fieldName = acf_banner_manager_field_name("search_{$fieldName}");
            $this->reference = 'options';
        } else
        if ($this->isSpecial('404') && $this->getSpecialValue('404', $fieldName)) {
            $this->fieldName = acf_banner_manager_field_name("404_{$fieldName}");
            $this->reference = 'options';
        } else
        if ($this->isTaxonomy() && $this->hasIndividualTaxonomy() && $this->getObjectValue($fieldName)) {
            $this->fieldName = $fieldName;
            $this->reference = get_queried_object();
        } else
        if ($this->isTaxonomy() && $this->hasGlobalTaxonomy() && $this->getTaxonomyValue($fieldName)) {
            $taxonomy = data_get(get_queried_object(), 'taxonomy');
            $this->fieldName = acf_banner_manager_field_name("taxonomy_{$taxonomy}_{$fieldName}");
            $this->reference = 'options';
        } else
        if (is_post_type_archive() && $this->hasArchiveGlobalPostType() && $this->getArchivePostTypeValue($fieldName)) {
            $postType = data_get(get_queried_object(), 'name');
            $this->fieldName = acf_banner_manager_field_name("post_type_{$postType}_{$fieldName}");
            $this->reference = 'options';
        } else
        if (is_home() && $this->hasIndividualPostType('page') && $this->getObjectValue($fieldName, $this->postsPage())) {
            $this->fieldName = $fieldName;
            $this->reference = $this->postsPage();
        } else
        if (is_front_page() && $this->hasIndividualPostType('page') && $this->getObjectValue($fieldName, $this->frontPage())) {
            $this->fieldName = $fieldName;
            $this->reference = $this->frontPage();
        } else
        if (is_singular() && $this->hasIndividualPostType() && $this->getObjectValue($fieldName)) {
            $this->fieldName = $fieldName;
            $this->reference = get_queried_object_id();
        } else
        if (is_singular() && $this->hasGlobalPostType() && $this->getPostTypeValue($fieldName)) {
            $postType = data_get(get_queried_object(), 'post_type');
            $this->fieldName = acf_banner_manager_field_name("post_type_{$postType}_{$fieldName}");
            $this->reference = 'options';
        } else
        if (true) {
            $this->fieldName = acf_banner_manager_field_name("default_{$fieldName}");
            $this->reference = 'options';
        }

        $filtered = apply_filters('acf/banner_manager/banner_field/field_name_reference', [
            'fieldName' => $this->fieldName,
            'reference' => $this->reference,
        ], $this->originalFieldName);

        $this->fieldName = data_get($filtered, 'fieldName');
        $this->reference = data_get($filtered, 'reference');
    }

    protected function isSpecial($type)
    {
        $function = "is_{$type}";
        return $function() && in_array($type, acf_banner_manager_get_value('enabled_special_type')? : []);
    }

    protected function getSpecialValue($type, $fieldName)
    {
        return acf_banner_manager_get_value("{$type}_{$fieldName}");
    }

    protected function isTaxonomy()
    {
        return is_tax() || is_category() || is_tag();
    }

    protected function hasGlobalTaxonomy()
    {
        return in_array(get_queried_object()->taxonomy, acf_banner_manager_get_value('global_taxonomies')? : []);
    }

    protected function getTaxonomyValue($fieldName)
    {
        $taxonomy = data_get(get_queried_object(), 'taxonomy');
        return acf_banner_manager_get_value("taxonomy_{$taxonomy}_{$fieldName}");
    }

    protected function hasIndividualTaxonomy()
    {
        return in_array(get_queried_object()->taxonomy, acf_banner_manager_get_value('individual_taxonomies')? : []);
    }

    protected function getObjectValue($fieldName, $reference = null)
    {
        return get_field($fieldName, $reference? : get_queried_object());
    }

    protected function hasGlobalPostType($postType = null)
    {
        return in_array($postType? : get_queried_object()->post_type, acf_banner_manager_get_value('global_post_types')? : []);
    }

    protected function getPostTypeValue($fieldName)
    {
        $postType = data_get(get_queried_object(), 'post_type');
        return acf_banner_manager_get_value("post_type_{$postType}_{$fieldName}");
    }

    protected function hasArchiveGlobalPostType()
    {
        return in_array(get_queried_object()->name, acf_banner_manager_get_value('global_post_types')? : []);
    }

    protected function getArchivePostTypeValue($fieldName)
    {
        $postType = data_get(get_queried_object(), 'name');
        return acf_banner_manager_get_value("post_type_{$postType}_{$fieldName}");
    }

    protected function hasIndividualPostType($postType = null)
    {
        return in_array(
            $postType? : get_queried_object()->post_type,
            acf_banner_manager_get_value('individual_post_types')? : []
        );
    }

    protected function frontPage()
    {
        return get_option('page_on_front');
    }

    protected function postsPage()
    {
        return get_option('page_for_posts');
    }
}
