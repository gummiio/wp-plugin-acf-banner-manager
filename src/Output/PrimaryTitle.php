<?php

namespace GummiIO\AcfBannerManager\Output;

class PrimaryTitle
{
    public function __construct()
    {
        add_filter('acf/banner_manager/banner_field_value', [$this, 'loadPrimaryTitle'], 10, 2);
    }

    public function loadPrimaryTitle($value, $outputter)
    {
        if ($value) {
            return $value;
        }

        if (! $this->isPrimaryTitleField($outputter)) {
            return $value;
        }

        return $this->primaryTitle($outputter);
    }

    public function primaryTitle($outputter)
    {
        $title = '';

        if (is_archive()) {
            $title = get_the_archive_title();
        }

        if (is_home()) {
            $title = get_the_title(get_option('page_for_posts'));
        }

        if (is_front_page()) {
            $title = get_the_title(get_option('page_on_front'));
        }

        if (is_singular()) {
            $title = get_the_title();
        }

        if (is_404()) {
            $title = __('Page Not Found', 'acf-banner_manager');
        }

        if (is_search()) {
            $title = sprintf('%s <span>%s</span>', __('Search for:'), get_search_query());
        }

        return apply_filters('acf/banner_manager/banner_field/primary_title', $title);
    }

    public function isPrimaryTitleField($outputter)
    {
        return $outputter->getOriginalField('key') == acf_banner_manager_get_value('primary_title_field');
    }
}
