<?php

namespace GummiIO\AcfBannerManager\Output;

class Shortcode
{
    public function __construct()
    {
        add_filter('acf/banner_manager/banner_field_value', [$this, 'runShortcode']);
    }

    public function runShortcode($value)
    {
        if (! acf_banner_manager_get_value('allow_shortcodes')) {
            return $value;
        }

        if (! is_string($value)) {
            return $value;
        }

        return do_shortcode($value);
    }
}
