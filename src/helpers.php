<?php

function acf_banner_manager($library = null) {
    global $acfBannerManager;

    if (! $library) {
        return $acfBannerManager;
    }

    return isset($acfBannerManager->$library)? $acfBannerManager->$library : null;
}

function acf_banner_manager_option_page($property = null) {
    $optionPage = acf_banner_manager('optionPage')->optionPage;

    return $property? data_get($optionPage, $property) : $optionPage;
}

function acf_banner_manager_option_group($property = null) {
    $optionGroup = acf_banner_manager('optionPage')->getOptionGroup('fieldGroup');

    return $property? data_get($optionGroup, $property) : $optionGroup;
}

function acf_banner_manager_banner_group($property = null) {
    if (! $bannerGroupKey = acf_banner_manager_get_value('banner_field_group')) {
        return null;
    }

    $bannerGroup = apply_filters('acf/banner_manager/banner_field_group', acf_get_field_group($bannerGroupKey));

    return $property? data_get($bannerGroup, $property) : $bannerGroup;
}

function acf_banner_manager_field_name($name, $type = '') {
    $prefix = apply_filters('acf/banner_manager/field_prefix', 'acf-banner_manager');

    return sprintf("%s%s-%s", $type, $prefix, sanitize_title($name));
}

function acf_banner_manager_get_value($name) {
    $value = get_field(acf_banner_manager_field_name($name), 'options');

    $value = apply_filters('acf/banner_manager/setting_value', $value, $name);
    $value = apply_filters("acf/banner_manager/setting_value/name={$name}", $value);

    return $value;
}

function acf_banner_manager_save_value($name, $value) {
    return update_field(acf_banner_manager_field_name($name), $value, 'options');
}

function acf_banner_manager_global_post_types() {
    $postTypes = acf_banner_manager_get_value('global_post_types')? : [];

    return apply_filters('acf/banner_manager/global_post_types', $postTypes);
}

function acf_banner_manager_global_taxonomies() {
    $taxonomies = acf_banner_manager_get_value('global_taxonomies')? : [];

    return apply_filters('acf/banner_manager/global_taxonomies', $taxonomies);
}

function acf_banner_manager_excluded_pages() {
    $pages = acf_banner_manager_get_value('excluded_pages')? : [];

    return apply_filters('acf/banner_manager/excluded_pages', $pages);
}

function acf_banner_manager_excluded_templates() {
    $templates = acf_banner_manager_get_value('excluded_templates')? : [];

    return apply_filters('acf/banner_manager/excluded_templates', $templates);
}

function banner_field_object($fieldName) {
    return new GummiIO\AcfBannerManager\Output\Output($fieldName);
}

function banner_field($fieldName) {
    echo get_banner_field($fieldName);
}

function get_banner_field($fieldName) {
    return banner_field_object($fieldName)->getValue();
}

function acf_banner_manager_merge_tags() {
    return apply_filters('acf/banner_manager/merge_tags', []);
}

function acf_banner_manager_add_terge_tag($key, $description, $callback) {
    add_filter('acf/banner_manager/merge_tags', function($mergeTags) use ($key, $description, $callback) {
        $mergeTags['{{'.$key.'}}'] = [
            'description' => $description,
            'callback' => $callback
        ];

        return $mergeTags;
    });
}
