<?php

namespace Tests;

trait AcfSupports
{
    protected function acfLocalGroup($args = [])
    {
        acf_add_local_field_group($args = $this->parseFieldGroupArgs($args));

        return acf_get_field_group($args['key']);
    }

    protected function acfDbGroup($args = [])
    {
        acf_update_field_group($args = $this->parseFieldGroupArgs($args));

        return acf_get_field_group($args['key']);
    }

    protected function acfLocalField($type, $name, $args = [], $group = null)
    {
        acf_add_local_field($args = $this->parseFieldArgs($type, $name, $args, $group? : $this->acfLocalGroup()));

        return acf_get_field($args['key']);
    }

    protected function acfDbField($type, $name, $args = [], $group = null)
    {
        acf_update_field($args = $this->parseFieldArgs($type, $name, $args, $group? : $this->acfDbGroup()));

        return acf_get_field($args['key']);
    }

    protected function acfClearLocals()
    {
        foreach (acf_local()->fields as $field) {
            acf_remove_local_field($field['key']);
        }
    }

    protected function newPostWithAcf($fields = [], $args = [])
    {
        $post = $this->factory->post->create_and_get($args);

        $this->insertObjectAcf($post, $fields);

        return $post;
    }

    protected function newTermWithAcf($fields = [], $args = [])
    {
        $term = $this->factory->term->create_and_get($args);

        $this->insertObjectAcf($term, $fields);

        return $term;
    }

    protected function insertObjectAcf($object, $fields = [])
    {
        foreach ($fields as $name => $value) {
            update_field($name, $value, $object);
        }
    }

    protected function parseFieldGroupArgs($args = [])
    {
        return wp_parse_args($args, [
            'key' => 'group_' . ($id = uniqid()),
            'title' => $id,
            'location' => [
                [
                    [
                        'param' => 'post_type',
                        'operator' => '==',
                        'value' => 'post',
                    ],
                ],
            ],
        ]);
    }

    protected function parseFieldArgs($type, $name, $args = [], $group = null)
    {
        return wp_parse_args($args, [
            'parent' => data_get($group, 'ID', $group['key']),
            'key'    => 'field_' . ($id = uniqid()),
            'title'  => $id,
            'name'   => $name,
            'type'   => $type,
        ]);
    }
}
