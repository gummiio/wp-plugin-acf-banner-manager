<?php

namespace Tests\Feature\Acf;

use Tests\TestCase;

class AcfExtendedTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->onOptionPage();

        $select = $this->acfLocalField('select', 'normal_select');

        $this->normalField = acf_prepare_field(acf_prepare_field($select));
    }

    /** @test */
    public function it_will_populate_field_groups_when_key_matches()
    {
        $select = $this->acfLocalField('select', 'select_test', [
            'acf_banner_manager_field_groups' => 1
        ]);

        $field = acf_prepare_field($select);

        $this->assertEmpty(data_get($this->normalField, 'choices'));
        $this->assertNotEmpty(data_get($field, 'choices'));
    }

    /** @test */
    public function it_will_exclude_setting_field_when_populating_field_groups()
    {
        $select = $this->acfLocalField('select', 'select_test', [
            'acf_banner_manager_field_groups' => 1
        ]);

        $field = acf_prepare_field($select);

        $this->assertArrayNotHasKey(data_get(acf_banner_manager_option_group(), 'key'), data_get($field, 'choices'));
    }

    /** @test */
    public function it_can_filter_populated_field_groups()
    {
        $select = $this->acfLocalField('select', 'select_test', [
            'acf_banner_manager_field_groups' => 1
        ]);

        add_filter('acf/banner_manager/field_groups_field_choices', function($choices) {
            $choices['test'] = 'works!';

            return $choices;
        });

        $field = acf_prepare_field($select);

        $this->assertArrayHasKey('test', data_get($field, 'choices'));
    }

    /** @test */
    public function it_will_populate_post_types_when_key_matches()
    {
        $postTypes = collect(get_post_types(['public' => true], 'objects'))
            ->pluck('label', 'name')
            ->all();

        $select = $this->acfLocalField('select', 'select_test', [
            'acf_banner_manager_post_types' => 1
        ]);

        $field = acf_prepare_field($select);

        $this->assertEmpty(data_get($this->normalField, 'choices'));
        $this->assertNotEmpty(data_get($field, 'choices'));
        $this->assertEquals($postTypes, data_get($field, 'choices'));
    }

    /** @test */
    public function it_can_filter_populated_post_types()
    {
        $select = $this->acfLocalField('select', 'select_test', [
            'acf_banner_manager_post_types' => 1
        ]);

        add_filter('acf/banner_manager/post_types_field_choices', function($choices) {
            $choices['test'] = 'works!';

            return $choices;
        });

        $field = acf_prepare_field($select);

        $this->assertArrayHasKey('test', data_get($field, 'choices'));
    }

    /** @test */
    public function it_will_populate_taxonomies_when_key_matches()
    {
        $taxonomiess = collect(get_taxonomies(['public' => true], 'objects'))
            ->pluck('label', 'name')
            ->all();

        $select = $this->acfLocalField('select', 'select_test', [
            'acf_banner_manager_taxonomies' => 1
        ]);

        $field = acf_prepare_field($select);

        $this->assertEmpty(data_get($this->normalField, 'choices'));
        $this->assertNotEmpty(data_get($field, 'choices'));
        $this->assertEquals($taxonomiess, data_get($field, 'choices'));
    }

    /** @test */
    public function it_can_filter_populated_taxonomies()
    {
        $select = $this->acfLocalField('select', 'select_test', [
            'acf_banner_manager_taxonomies' => 1
        ]);

        add_filter('acf/banner_manager/taxonomies_field_choices', function($choices) {
            $choices['test'] = 'works!';

            return $choices;
        });

        $field = acf_prepare_field($select);

        $this->assertArrayHasKey('test', data_get($field, 'choices'));
    }

    /** @test */
    public function it_should_not_show_banner_gruop_field_on_none_setting_page()
    {
        $fieldGroups = acf_get_field_groups();

        $_GET['page'] = acf_banner_manager_option_page('menu_slug');
        $settingFieldGroups = acf_get_field_groups();

        $optionGroupKey = acf_banner_manager_option_group('key');
        $this->assertFalse(collect($fieldGroups)->pluck('key')->contains($optionGroupKey));
        $this->assertTrue(collect($settingFieldGroups)->pluck('key')->contains($optionGroupKey));
    }

    /** @test */
    public function it_will_add_merge_tags_tool_tip_when_class_matches()
    {
        $bannerGroup = $this->acfDbGroup();
        $text = $this->acfLocalField('text', 'test', [], $bannerGroup);
        acf_banner_manager_save_value('enable_merge_tags', true);
        acf_banner_manager_save_value('banner_merge_tag_fields', [$text['key']]);

        $field = acf_prepare_field($text);

        $this->assertNotRegExp('/acf-js-tooltip/', data_get($this->normalField, 'label'));
        $this->assertRegExp('/acf-js-tooltip/', data_get($field, 'label'));
    }

    /** @test */
    public function it_will_indicate_the_banner_group_that_is_selected()
    {
        set_current_screen('edit-acf-field-group');

        $bannerGroup = $this->acfDbGroup();
        $bannerGroupPost = get_post(data_get($bannerGroup, 'ID'));
        $post = $this->factory->post->create_and_get();
        acf_banner_manager_save_value('banner_field_group', data_get($bannerGroup, 'key'));

        $this->assertRegExp('/Banner Group/', $this->getOutput('_post_states', [$bannerGroupPost]));
        $this->assertNotRegExp('/Banner Group/', $this->getOutput('_post_states', [$post]));
    }

    /** @test */
    public function it_will_not_indicate_the_banner_group_on_none_edit_page()
    {
        set_current_screen('edit-post');

        $bannerGroup = $this->acfDbGroup();
        $bannerGroupPost = get_post(data_get($bannerGroup, 'ID'));
        acf_banner_manager_save_value('field_group', data_get($bannerGroup, 'key'));

        $this->assertNotRegExp('/Banner Group/', $this->getOutput('_post_states', [$bannerGroupPost]));
    }

    /** @test */
    public function it_will_populate_page_templates_when_key_matches()
    {
        $templates = array_merge([
            'default' => apply_filters('default_page_template_title',  __('Default Template', 'acf'))
        ], wp_get_theme()->get_page_templates());

        $select = $this->acfLocalField('select', 'select_test', [
            'acf_banner_manager_page_templates' => 1
        ]);

        $field = acf_prepare_field($select);

        $this->assertEmpty(data_get($this->normalField, 'choices'));
        $this->assertNotEmpty(data_get($field, 'choices'));
        $this->assertEquals($templates, data_get($field, 'choices'));
    }

    /** @test */
    public function it_can_filter_populated_page_templates()
    {
        $select = $this->acfLocalField('select', 'select_test', [
            'acf_banner_manager_page_templates' => 1
        ]);

        add_filter('acf/banner_manager/page_templates_field_choices', function($choices) {
            $choices['test'] = 'works!';

            return $choices;
        });

        $field = acf_prepare_field($select);

        $this->assertArrayHasKey('test', data_get($field, 'choices'));
    }

    /** @test */
    public function it_will_populate_text_fields_from_banner_group()
    {
        $bannerGroup = $this->acfDbGroup();
        acf_banner_manager_save_value('banner_field_group', data_get($bannerGroup, 'key'));
        $textField = $this->acfDbField('text', 'text_field', [], $bannerGroup);
        $imageField = $this->acfDbField('image', 'image_field', [], $bannerGroup);

        $select = $this->acfLocalField('select', 'select_test', [
            'acf_banner_manager_banner_group_fields' => 1
        ]);

        $select2 = $this->acfLocalField('select', 'select_test', [
            'acf_banner_manager_banner_group_fields' => ['text', 'image']
        ]);

        $field = acf_prepare_field($select);
        $field2 = acf_prepare_field($select2);

        $this->assertArrayHasKey(data_get($textField, 'key'), data_get($field, 'choices'));
        $this->assertArrayNotHasKey(data_get($imageField, 'key'), data_get($field, 'choices'));

        $this->assertArrayHasKey(data_get($textField, 'key'), data_get($field2, 'choices'));
        $this->assertArrayHasKey(data_get($imageField, 'key'), data_get($field2, 'choices'));
    }

    /** @test */
    public function it_will_add_instructions_on_primary_banner_field()
    {
        $primaryField = $this->acfDbField('text', 'text_field');
        $nonePrimaryField = $this->acfDbField('text', 'text_field_2');
        acf_banner_manager_save_value('primary_title_field', data_get($primaryField, 'key'));

        $field = acf_prepare_field($primaryField);
        $field2 = acf_prepare_field($nonePrimaryField);

        $this->assertNotEquals('', data_get($field, 'instructions'));
        $this->assertEquals('', data_get($field2, 'instructions'));
    }
}
