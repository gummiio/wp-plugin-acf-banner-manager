<?php

namespace Tests\Feature\Acf;

use GummiIO\AcfBannerManager\Acf\OptionGroup;
use Tests\TestCase;

class OptionPageTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->onOptionPage();
    }

    /** @test */
    public function it_will_register_option_page()
    {
        $this->assertSame(true, !! has_action('acf/init', [acf_banner_manager('optionPage'), 'addOptionPage']));
        $this->assertNotNull(acf_banner_manager_option_page());
    }

    /** @test */
    public function it_register_option_paage_field_group_fields()
    {
        $this->assertSame(true, !! has_action('acf/init', [acf_banner_manager('optionPage'), 'registerOptionPageFields']));

        $this->assertInstanceOf(OptionGroup::class, acf_banner_manager('optionPage')->getOptionGroup());
    }

    /** @test */
    public function it_will_generate_default_banner_group_if_auto_generate_is_checked()
    {
        acf_banner_manager_save_value('generate_field_group', true);

        do_action('acf/save_post', 'options');

        $bannerFieldGroupKey = acf_banner_manager_banner_group('key');

        $this->assertTrue(collect(acf_get_field_groups())->pluck('key')->contains($bannerFieldGroupKey));
        $this->assertFalse(!! acf_banner_manager_get_value('generate_field_group'));
    }

    /** @test */
    public function it_will_generate_new_banner_group_regarless_if_old_one_is_set()
    {
        $oldBannerGroup = $this->acfDbGroup();
        acf_banner_manager_save_value('banner_field_group', data_get($oldBannerGroup, 'key'));
        acf_banner_manager_save_value('generate_field_group', true);

        do_action('acf/save_post', 'options');

        $this->assertNotEquals(data_get($oldBannerGroup, 'key'), acf_banner_manager_banner_group('key'));
    }

    /** @test */
    public function it_will_not_generate_new_banner_group_if_not_on_setting_page()
    {
        $this->onOptionPage('some_random_page');

        $allFieldGroups = acf_get_field_groups();
        acf_banner_manager_save_value('generate_field_group', true);

        do_action('acf/save_post', 'options');

        $this->assertCount(count($allFieldGroups), acf_get_field_groups());
    }

    /** @test */
    public function it_will_not_generate_new_banner_group_if_auto_generate_not_check()
    {
        $allFieldGroups = acf_get_field_groups();
        acf_banner_manager_save_value('generate_field_group', false);

        do_action('acf/save_post', 'options');

        $this->assertCount(count($allFieldGroups), acf_get_field_groups());
    }

    /** @test */
    public function it_will_update_banner_group_location_when_save()
    {
        acf_banner_manager_save_value('banner_field_group', data_get($this->acfDbGroup(), 'key'));
        acf_banner_manager_save_value('individual_post_types', ['page']);
        acf_banner_manager_save_value('individual_taxonomies', ['category']);

        do_action('acf/save_post', 'options');

        $locationsFlat = collect(acf_banner_manager_banner_group('location'))->flatten(1)->all();
        $pageLocation = ['param' => 'post_type', 'operator' => '==', 'value' => 'page'];
        $taxonomyLocation = ['param' => 'taxonomy', 'operator' => '==', 'value' => 'category'];

        $this->assertContains($pageLocation, $locationsFlat);
        $this->assertContains($taxonomyLocation, $locationsFlat);
    }

    /** @test */
    public function it_will_not_update_banner_group_location_if_not_on_banner_option_page()
    {
        $this->onOptionPage('some-random-page');

        acf_banner_manager_save_value('banner_field_group', data_get($this->acfDbGroup(['location' => []]), 'key'));

        do_action('acf/save_post', 'options');

        $this->assertEmpty(acf_banner_manager_banner_group('location'));
    }

    /** @test */
    public function it_will_update_banner_group_location_by_excluded_templates_when_save()
    {
        acf_banner_manager_save_value('banner_field_group', data_get($this->acfDbGroup(), 'key'));
        acf_banner_manager_save_value('individual_post_types', ['page']);
        acf_banner_manager_save_value('excluded_templates', ['abc', 'def']);

        do_action('acf/save_post', 'options');

        $locations = acf_banner_manager_banner_group('location');
        $pageLocation = [['param' => 'post_type', 'operator' => '==', 'value' => 'page']];
        $excludePageLocation = [
            ['param' => 'post_type', 'operator' => '==', 'value' => 'page'],
            ['param' => 'page_template', 'operator' => '!=', 'value' => 'abc'],
            ['param' => 'page_template', 'operator' => '!=', 'value' => 'def']
        ];

        $this->assertNotContains($pageLocation, $locations);
        $this->assertContains($excludePageLocation, $locations);
    }

    /** @test */
    public function it_will_update_banner_group_location_with_regular_page_if_no_exlucded_template_is_selected()
    {
        acf_banner_manager_save_value('banner_field_group', data_get($this->acfDbGroup(), 'key'));
        acf_banner_manager_save_value('individual_post_types', ['page']);
        acf_banner_manager_save_value('excluded_templates', []);

        do_action('acf/save_post', 'options');

        $locations = acf_banner_manager_banner_group('location');
        $pageLocation = [['param' => 'post_type', 'operator' => '==', 'value' => 'page']];

        $this->assertContains($pageLocation, $locations);
    }

    /** @test */
    public function it_will_not_update_banner_group_location_by_excluded_template_if_page_is_not_selected()
    {
        acf_banner_manager_save_value('banner_field_group', data_get($this->acfDbGroup(), 'key'));
        acf_banner_manager_save_value('individual_post_types', []);
        acf_banner_manager_save_value('excluded_templates', [1, 2]);

        do_action('acf/save_post', 'options');

        $locations = acf_banner_manager_banner_group('location');
        $excludePageLocation = [
            ['param' => 'post_type', 'operator' => '==', 'value' => 'page'],
            ['param' => 'page_template', 'operator' => '!=', 'value' => 'abc'],
            ['param' => 'page_template', 'operator' => '!=', 'value' => 'def']
        ];

        $this->assertNotContains($excludePageLocation, $locations);
    }

    /** @test */
    public function it_can_filter_generated_banner_group_location()
    {
        acf_banner_manager_save_value('banner_field_group', data_get($this->acfDbGroup(), 'key'));
        acf_banner_manager_save_value('individual_post_types', ['page']);

        add_filter('acf/banner_manager/banner_group_location', '__return_empty_array');

        do_action('acf/save_post', 'options');

        $this->assertEmpty(acf_banner_manager_banner_group('location'));
    }

    /** @test */
    public function it_will_not_attempt_to_generate_location_and_create_new_field_group_if_no_banner_group_is_chosen()
    {
        $fieldGroups = acf_get_field_groups();
        acf_banner_manager_save_value('individual_post_types', ['page']);
        do_action('acf/save_post', 'options');

        $this->assertEquals($fieldGroups, acf_get_field_groups());
    }

    /** @test */
    public function it_can_add_more_default_banner_fields()
    {
        acf_banner_manager_save_value('generate_field_group', true);

        add_action('acf/banner_manager/default_field_group', function($fieldGroupID) {
            acf_update_field([
                'parent'       => $fieldGroupID,
                'key'          => uniqid('field_'),
                'label'        => 'Custom Field',
                'name'         => 'custom_field',
                'type'         => 'text',
            ]);
        });

        do_action('acf/save_post', 'options');

        $bannerFieldGroupKey = acf_banner_manager_banner_group('key');
        $this->assertTrue(collect(acf_get_fields($bannerFieldGroupKey))->pluck('name')->contains('custom_field'));
    }
}
