<?php

namespace Tests\Feature\Core;

use Tests\TestCase;

class AdminTest extends TestCase
{
    /** @test */
    public function it_will_include_custom_css_on_admin_page()
    {
        $this->assertSame(true, !! has_action('admin_enqueue_scripts', [acf_banner_manager('admin'), 'enqueyeSripts']));
    }
}
