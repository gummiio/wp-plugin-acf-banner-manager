<?php

namespace Tests\Feature\Core;

use Tests\TestCase;

class MergeTagTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        $this->post = $this->factory->post->create_and_get(['post_author' => 1]);
        $this->category = $this->factory->term->create_and_get(['taxonomy' => 'category', 'description' => 'abc']);
        add_shortcode('asdf', function() { return 'qwer'; });

        $this->bannerTitleField = $this->acfDbField('text', 'banner_title', [], $bannerGroup = $this->acfDbGroup());
        $this->bannerContentField = $this->acfDbField('textarea', 'banner_content', [], $bannerGroup);
        acf_banner_manager_save_value('banner_field_group', data_get($bannerGroup, 'key'));
        acf_banner_manager_save_value('banner_merge_tag_fields', [data_get($this->bannerTitleField, 'key')]);
        acf_banner_manager_save_value('default_banner_title', 'default value');
    }

    /** @test */
    public function it_will_run_shortcode_if_its_enabled()
    {
        acf_banner_manager_save_value('default_banner_title', '[asdf]');
        $this->assertEquals('[asdf]', get_banner_field('banner_title'));

        acf_banner_manager_save_value('allow_shortcodes', true);
        $this->assertEquals('qwer', get_banner_field('banner_title'));
    }

    /** @test */
    public function it_will_run_merge_tags_if_enabled()
    {
        $this->go_to(get_permalink($this->post));

        acf_banner_manager_save_value('enable_merge_tags', false);
        acf_banner_manager_save_value('default_banner_title', '{{page_title}}');
        acf_banner_manager_save_value('default_banner_content', '{{page_title}}');

        $this->assertEquals('{{page_title}}', get_banner_field('banner_title'));
        $this->assertEquals('{{page_title}}', get_banner_field('banner_content'));

        acf_banner_manager_save_value('enable_merge_tags', true);
        $this->assertEquals($this->post->post_title, get_banner_field('banner_title'));
        $this->assertEquals('{{page_title}}', get_banner_field('banner_content'));
    }

    /** @test */
    public function it_will_render_proper_post_merge_tags_value()
    {
        acf_banner_manager_save_value('enable_merge_tags', true);

        $this->go_to(get_permalink($this->post));
        $this->assertTrue(is_singular());
        acf_banner_manager_save_value('default_banner_title', '{{page_title}}');
        $this->assertEquals($this->post->post_title, get_banner_field('banner_title'));
    }

    /** @test */
    public function it_will_render_proper_taxonomy_term_merge_tags_value()
    {
        acf_banner_manager_save_value('enable_merge_tags', true);

        $this->go_to(get_term_link($this->category));
        $this->assertTrue(is_category());

        acf_banner_manager_save_value('default_banner_title', '{{term_name}}');
        $this->assertEquals($this->category->name, get_banner_field('banner_title'));

        acf_banner_manager_save_value('default_banner_title', '{{term_description}}');
        $this->assertEquals('abc', get_banner_field('banner_title'));

        acf_banner_manager_save_value('default_banner_title', '{{taxonomy_name}}');
        $this->assertEquals('Category', get_banner_field('banner_title'));
    }

    /** @test */
    public function it_will_render_proper_search_merge_tags_value()
    {
        acf_banner_manager_save_value('enable_merge_tags', true);

        $this->go_to(home_url('?s=asdf'));
        $this->assertTrue(is_search());

        acf_banner_manager_save_value('default_banner_title', '{{search_query}}');
        $this->assertEquals('{{search_query}}', get_banner_field('banner_title'));

        acf_banner_manager_save_value('enabled_special_type', ['search']);
        $this->assertEquals('asdf', get_banner_field('banner_title'));
    }


    /** @test */
    public function it_will_render_proper_author_merge_tags_value()
    {
        acf_banner_manager_save_value('enable_merge_tags', true);

        $this->go_to(get_author_posts_url(get_the_author_meta('ID', $this->post->post_author)));
        $this->assertTrue(is_author());

        acf_banner_manager_save_value('default_banner_title', '{{author_name}}');
        $this->assertEquals('{{author_name}}', get_banner_field('banner_title'));

        acf_banner_manager_save_value('enabled_special_type', ['author']);
        $this->assertEquals('admin', get_banner_field('banner_title'));
    }

    /** @test */
    public function it_will_render_proper_day_merge_tags_value()
    {
        acf_banner_manager_save_value('enable_merge_tags', true);

        $this->go_to(get_day_link('', '', ''));
        $this->assertTrue(is_day());

        acf_banner_manager_save_value('default_banner_title', '{{day}}');
        $this->assertEquals('{{day}}', get_banner_field('banner_title'));

        acf_banner_manager_save_value('enabled_special_type', ['day']);
        $this->assertEquals(date('d'), get_banner_field('banner_title'));
    }

    /** @test */
    public function it_will_render_proper_month_merge_tags_value()
    {
        acf_banner_manager_save_value('enable_merge_tags', true);

        $this->go_to(get_month_link('', ''));
        $this->assertTrue(is_month());

        acf_banner_manager_save_value('default_banner_title', '{{month}}');
        $this->assertEquals('{{month}}', get_banner_field('banner_title'));
        acf_banner_manager_save_value('default_banner_title', '{{day}}');
        $this->assertEquals('{{day}}', get_banner_field('banner_title'));

        acf_banner_manager_save_value('enabled_special_type', ['month']);
        acf_banner_manager_save_value('default_banner_title', '{{month}}');
        $this->assertEquals(date('F'), get_banner_field('banner_title'));
        acf_banner_manager_save_value('default_banner_title', '{{day}}');
        $this->assertEquals(date('d'), get_banner_field('banner_title'));
    }

    /** @test */
    public function it_will_render_proper_year_merge_tags_value()
    {
        acf_banner_manager_save_value('enable_merge_tags', true);

        $this->go_to(get_year_link('', ''));
        $this->assertTrue(is_year());

        acf_banner_manager_save_value('default_banner_title', '{{year}}');
        $this->assertEquals('{{year}}', get_banner_field('banner_title'));
        acf_banner_manager_save_value('default_banner_title', '{{month}}');
        $this->assertEquals('{{month}}', get_banner_field('banner_title'));
        acf_banner_manager_save_value('default_banner_title', '{{day}}');
        $this->assertEquals('{{day}}', get_banner_field('banner_title'));

        acf_banner_manager_save_value('enabled_special_type', ['year']);
        acf_banner_manager_save_value('default_banner_title', '{{year}}');
        $this->assertEquals(date('Y'), get_banner_field('banner_title'));
        acf_banner_manager_save_value('default_banner_title', '{{month}}');
        $this->assertEquals(date('F'), get_banner_field('banner_title'));
        acf_banner_manager_save_value('default_banner_title', '{{day}}');
        $this->assertEquals(date('d'), get_banner_field('banner_title'));
    }

    /** @test */
    public function it_will_allow_custom_merge_tag()
    {
        add_filter('acf/banner_manager/merge_tags', function($mergeTags) {
            $mergeTags['//haha//'] = [
                'description' => 'asdf',
                'callback' => function() {
                    return 'yaya';
                }
            ];

            return $mergeTags;
        });

        acf_banner_manager_save_value('enable_merge_tags', true);
        acf_banner_manager_save_value('default_banner_title', '//haha//');
        $this->assertEquals('yaya', get_banner_field('banner_title'));
    }
}
