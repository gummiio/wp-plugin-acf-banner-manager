<?php

namespace Tests\Feature\Core;

use Tests\TestCase;

class OutputTest extends TestCase
{
    public function setUp()
    {
        parent::setUp();

        set_current_screen('front');
        $this->set_permalink_structure('/%postname%');
        $this->post = $this->factory->post->create_and_get(['post_author' => 1]);

        $this->bannerTitleField = $this->acfDbField('text', 'banner_title', [], $bannerGroup = $this->acfDbGroup());
        acf_banner_manager_save_value('banner_field_group', data_get($bannerGroup, 'key'));

        acf_banner_manager_save_value('primary_title_field', data_get($this->bannerTitleField, 'key'));
    }

    /** @test */
    public function it_will_load_proper_day_value_with_fallback_if_it_is_on_day_archive()
    {
        acf_banner_manager_save_value('day_banner_title', 'day value');

        $this->go_to(get_day_link('', '', ''));
        $this->assertTrue(is_day());

        acf_banner_manager_save_value('enabled_special_type', []);
        $this->assertEquals(get_the_archive_title(), get_banner_field('banner_title'));

        acf_banner_manager_save_value('default_banner_title', 'default value');
        $this->assertEquals('default value', get_banner_field('banner_title'));

        acf_banner_manager_save_value('enabled_special_type', ['day']);
        $this->assertEquals('day value', get_banner_field('banner_title'));

        acf_banner_manager_save_value('day_banner_title', '');
        $this->assertEquals('default value', get_banner_field('banner_title'));
    }

    /** @test */
    public function it_will_load_proper_month_value_with_fallback_if_it_is_on_month_archive()
    {
        acf_banner_manager_save_value('month_banner_title', 'month value');

        $this->go_to(get_month_link('', ''));
        $this->assertTrue(is_month());

        acf_banner_manager_save_value('enabled_special_type', []);
        $this->assertEquals(get_the_archive_title(), get_banner_field('banner_title'));

        acf_banner_manager_save_value('default_banner_title', 'default value');
        $this->assertEquals('default value', get_banner_field('banner_title'));

        acf_banner_manager_save_value('enabled_special_type', ['month']);
        $this->assertEquals('month value', get_banner_field('banner_title'));

        acf_banner_manager_save_value('month_banner_title', '');
        $this->assertEquals('default value', get_banner_field('banner_title'));
    }

    /** @test */
    public function it_will_load_proper_year_value_with_fallback_if_it_is_on_year_archive()
    {
        acf_banner_manager_save_value('year_banner_title', 'year value');

        $this->go_to(get_year_link(''));
        $this->assertTrue(is_year());

        acf_banner_manager_save_value('enabled_special_type', []);
        $this->assertEquals(get_the_archive_title(), get_banner_field('banner_title'));

        acf_banner_manager_save_value('default_banner_title', 'default value');
        $this->assertEquals('default value', get_banner_field('banner_title'));

        acf_banner_manager_save_value('enabled_special_type', ['year']);
        $this->assertEquals('year value', get_banner_field('banner_title'));

        acf_banner_manager_save_value('year_banner_title', '');
        $this->assertEquals('default value', get_banner_field('banner_title'));
    }

    /** @test */
    public function it_will_load_proper_author_value_with_fallback_if_it_is_on_author_archive()
    {
        acf_banner_manager_save_value('author_banner_title', 'author value');

        $this->go_to(get_author_posts_url(get_the_author_meta('ID', $this->post->post_author)));
        $this->assertTrue(is_author());

        acf_banner_manager_save_value('enabled_special_type', []);
        $this->assertEquals(get_the_archive_title(), get_banner_field('banner_title'));

        acf_banner_manager_save_value('default_banner_title', 'default value');
        $this->assertEquals('default value', get_banner_field('banner_title'));

        acf_banner_manager_save_value('enabled_special_type', ['author']);
        $this->assertEquals('author value', get_banner_field('banner_title'));

        acf_banner_manager_save_value('author_banner_title', '');
        $this->assertEquals('default value', get_banner_field('banner_title'));
    }

    /** @test */
    public function it_will_load_proper_search_value_with_fallback_if_it_is_on_search_archive()
    {
        acf_banner_manager_save_value('search_banner_title', 'search value');

        $this->go_to(get_search_link('testing'));
        $this->assertTrue(is_search());

        acf_banner_manager_save_value('enabled_special_type', []);
        $this->assertEquals('Search for: <span>testing</span>', get_banner_field('banner_title'));

        acf_banner_manager_save_value('default_banner_title', 'default value');
        $this->assertEquals('default value', get_banner_field('banner_title'));

        acf_banner_manager_save_value('enabled_special_type', ['search']);
        $this->assertEquals('search value', get_banner_field('banner_title'));

        acf_banner_manager_save_value('search_banner_title', '');
        $this->assertEquals('default value', get_banner_field('banner_title'));
    }

    /** @test */
    public function it_will_load_proper_404_value_with_fallback_if_it_is_on_404_archive()
    {
        acf_banner_manager_save_value('404_banner_title', '404 value');

        $this->go_to('/notapage');
        $this->assertTrue(is_404());

        acf_banner_manager_save_value('enabled_special_type', []);
        $this->assertEquals('Page Not Found', get_banner_field('banner_title'));

        acf_banner_manager_save_value('default_banner_title', 'default value');
        $this->assertEquals('default value', get_banner_field('banner_title'));

        acf_banner_manager_save_value('enabled_special_type', ['404']);
        $this->assertEquals('404 value', get_banner_field('banner_title'));

        acf_banner_manager_save_value('404_banner_title', '');
        $this->assertEquals('default value', get_banner_field('banner_title'));
    }

    /** @test */
    public function it_will_load_proper_category_value_with_fallback_if_it_is_on_category_archive()
    {
        $category = $this->factory->term->create_and_get(['taxonomy' => 'category']);
        acf_banner_manager_save_value('taxonomy_category_banner_title', 'global category value');
        update_field('banner_title', 'category value', $category);

        $this->go_to(get_term_link($category));
        $this->assertTrue(is_category());
        $this->assertEquals(get_the_archive_title(), get_banner_field('banner_title'));

        acf_banner_manager_save_value('default_banner_title', 'default value');
        $this->assertEquals('default value', get_banner_field('banner_title'));

        acf_banner_manager_save_value('global_taxonomies', ['category']);
        $this->assertEquals('global category value', get_banner_field('banner_title'));

        acf_banner_manager_save_value('individual_taxonomies', ['category']);
        $this->assertEquals('category value', get_banner_field('banner_title'));

        update_field('banner_title', '', $category);
        $this->assertEquals('global category value', get_banner_field('banner_title'));

        acf_banner_manager_save_value('taxonomy_category_banner_title', '');
        $this->assertEquals('default value', get_banner_field('banner_title'));
    }

    /** @test */
    public function it_will_load_proper_custom_taxonomy_value_with_fallback_if_it_is_on_custom_taxonomy_archive()
    {
        register_taxonomy('type', 'post');
        delete_option('rewrite_rules');
        $type = $this->factory->term->create_and_get(['taxonomy' => 'type']);
        $post = $this->factory->post->create(['tax_input' => ['type' => $type->term_id]]);
        acf_banner_manager_save_value('taxonomy_type_banner_title', 'global type value');
        update_field('banner_title', 'type value', $type);

        $this->go_to(get_term_link($type));
        $this->assertTrue(is_tax());
        $this->assertEquals(get_the_archive_title(), get_banner_field('banner_title'));

        acf_banner_manager_save_value('default_banner_title', 'default value');
        $this->assertEquals('default value', get_banner_field('banner_title'));

        acf_banner_manager_save_value('global_taxonomies', ['type']);
        $this->assertEquals('global type value', get_banner_field('banner_title'));

        acf_banner_manager_save_value('individual_taxonomies', ['type']);
        $this->assertEquals('type value', get_banner_field('banner_title'));

        update_field('banner_title', '', $type);
        $this->assertEquals('global type value', get_banner_field('banner_title'));

        acf_banner_manager_save_value('taxonomy_type_banner_title', '');
        $this->assertEquals('default value', get_banner_field('banner_title'));
    }

    /** @test */
    public function it_will_load_proper_page_value_with_fallback_if_it_is_on_page_archive()
    {
        $page = $this->factory->post->create(['post_type' => 'page']);
        acf_banner_manager_save_value('individual_post_types', []); // reset the default
        acf_banner_manager_save_value('post_type_page_banner_title', 'global page value');
        update_field('banner_title', 'page value', $page);

        update_option('show_on_front', 'page');
        update_option('page_for_posts', $page);
        $this->go_to(get_permalink($page));
        $this->assertTrue(is_home());
        $this->assertEquals(get_the_title($page), get_banner_field('banner_title'));

        update_option('page_on_front', $page);
        $this->go_to(get_permalink($page));
        $this->assertTrue(is_front_page());
        $this->assertEquals(get_the_title($page), get_banner_field('banner_title'));

        delete_option('page_for_posts');
        delete_option('page_on_front');
        $this->go_to(get_permalink($page));
        $this->assertTrue(is_page() && ! is_home() && ! is_front_page());
        $this->assertEquals(get_the_title($page), get_banner_field('banner_title'));

        acf_banner_manager_save_value('default_banner_title', 'default value');
        $this->assertEquals('default value', get_banner_field('banner_title'));

        acf_banner_manager_save_value('global_post_types', ['page']);
        $this->assertEquals('global page value', get_banner_field('banner_title'));

        acf_banner_manager_save_value('individual_post_types', ['page']);
        $this->assertEquals('page value', get_banner_field('banner_title'));

        update_field('banner_title', '', $page);
        $this->assertEquals('global page value', get_banner_field('banner_title'));

        acf_banner_manager_save_value('post_type_page_banner_title', '');
        $this->assertEquals('default value', get_banner_field('banner_title'));
    }

    /** @test */
    public function it_will_load_proper_post_type_value_with_fallback_if_it_is_on_post_type_archive()
    {
        register_post_type('service', ['public' => true, 'has_archive' => true]);
        delete_option('rewrite_rules');
        $service = $this->factory->post->create(['post_type' => 'service']);
        acf_banner_manager_save_value('individual_post_types', []); // reset the default
        acf_banner_manager_save_value('post_type_service_banner_title', 'global service value');
        update_field('banner_title', 'service value', $service);

        $this->go_to(get_post_type_archive_link('service'));
        $this->assertTrue(is_post_type_archive('service'));
        $this->assertEquals(get_the_archive_title(), get_banner_field('banner_title'));

        acf_banner_manager_save_value('default_banner_title', 'default value');
        $this->assertEquals('default value', get_banner_field('banner_title'));

        acf_banner_manager_save_value('global_post_types', ['service']);
        $this->assertEquals('global service value', get_banner_field('banner_title'));

        acf_banner_manager_save_value('individual_post_types', ['service']);
        $this->assertEquals('global service value', get_banner_field('banner_title'));

        acf_banner_manager_save_value('post_type_service_banner_title', '');
        $this->assertEquals('default value', get_banner_field('banner_title'));

        acf_banner_manager_save_value('global_post_types', []);
        acf_banner_manager_save_value('individual_post_types', []);
        acf_banner_manager_save_value('default_banner_title', '');
        acf_banner_manager_save_value('post_type_service_banner_title', 'global service value');
        $this->go_to(get_permalink($service));
        $this->assertTrue(is_singular());
        $this->assertEquals(get_the_title($service), get_banner_field('banner_title'));

        acf_banner_manager_save_value('default_banner_title', 'default value', 'options');
        $this->assertEquals('default value', get_banner_field('banner_title'));

        acf_banner_manager_save_value('global_post_types', ['service']);
        $this->assertEquals('global service value', get_banner_field('banner_title'));

        acf_banner_manager_save_value('individual_post_types', ['service']);
        $this->assertEquals('service value', get_banner_field('banner_title'));

        update_field('banner_title', '', $service);
        $this->assertEquals('global service value', get_banner_field('banner_title'));

        acf_banner_manager_save_value('post_type_service_banner_title', '');
        $this->assertEquals('default value', get_banner_field('banner_title'));
    }

    /** @test */
    public function it_allows_custom_filters_on_the_field_name_and_reference()
    {
        acf_banner_manager_save_value('asdf_banner_title', 'custom value');

        add_filter('acf/banner_manager/banner_field/field_name_reference', function($fieldName, $input) {
            return [
                'fieldName' => acf_banner_manager_field_name("asdf_{$input}"),
                'reference' => 'options'
            ];
        }, 10, 2);

        $this->assertEquals('custom value', get_banner_field('banner_title'));
    }

    /** @test */
    public function it_can_apply_filter_directly_after_value_is_retreived()
    {
        add_filter('acf/banner_manager/banner_field_value', function($value) {
            return "overwrite me";
        });

        $this->assertEquals('overwrite me', get_banner_field('banner_title'));
    }
}
