<?php

namespace Tests\Feature;

use Tests\TestCase;

class HelperTest extends TestCase
{
    /** @test */
    public function it_can_grab_the_banner_manager_instance_or_its_libraries()
    {
        global $acfBannerManager;

        $this->assertSame($acfBannerManager, acf_banner_manager());
        $this->assertSame($acfBannerManager->updater, acf_banner_manager('updater'));
        $this->assertSame($acfBannerManager->admin, acf_banner_manager('admin'));
        $this->assertSame($acfBannerManager->acfExtended, acf_banner_manager('acfExtended'));
        $this->assertSame($acfBannerManager->optionPage, acf_banner_manager('optionPage'));
    }

    /** @test */
    public function it_can_grab_option_page_or_its_property()
    {
        $optionPageSlug = 'acf-options-' . sanitize_title('banner');
        $optionPage = acf_options_page()->get_page($optionPageSlug);

        $this->assertEquals($optionPage, acf_banner_manager_option_page());
        $this->assertEquals($optionPage['menu_slug'], acf_banner_manager_option_page('menu_slug'));
    }

    /** @test */
    public function it_can_grab_option_page_field_group_or_its_property()
    {
        $fieldGroup = acf_get_field_group('group_acf-banner_manager-settings');

        $this->assertEquals($fieldGroup, acf_banner_manager_option_group());
        $this->assertEquals($fieldGroup['key'], acf_banner_manager_option_group('key'));
    }

    /** @test */
    public function it_can_grab_selected_banner_field_group_or_its_property()
    {
        $bannerGroup = $this->acfLocalGroup();
        acf_banner_manager_save_value('banner_field_group', $bannerGroup['key']);

        $this->assertEquals($bannerGroup, acf_banner_manager_banner_group());
        $this->assertEquals($bannerGroup['key'], acf_banner_manager_banner_group('key'));
    }

    /** @test */
    public function it_can_filter_banner_field_group()
    {
        $bannerGroup = $this->acfLocalGroup();
        acf_banner_manager_save_value('banner_field_group', $bannerGroup['key']);

        $overwriteBannerGroup = $this->acfLocalGroup();
        add_filter('acf/banner_manager/banner_field_group', function() use ($overwriteBannerGroup) {
            return $overwriteBannerGroup;
        });

        $this->assertEquals($overwriteBannerGroup, acf_banner_manager_banner_group());
    }

    /** @test */
    public function it_will_return_null_if_no_banner_field_group_is_selected()
    {
        acf_banner_manager_save_value('banner_field_group', null);

        $this->assertNull(acf_banner_manager_banner_group());
    }

    /** @test */
    public function it_will_prefix_setting_field_name()
    {
        $this->assertEquals('acf-banner_manager-test', acf_banner_manager_field_name('test'));
    }

    /** @test */
    public function it_will_prefix_setting_field_name_with_another_prefix()
    {
        $this->assertEquals('field_acf-banner_manager-test', acf_banner_manager_field_name('test', 'field_'));
        $this->assertEquals('group_acf-banner_manager-test', acf_banner_manager_field_name('test', 'group_'));
    }

    /** @test */
    public function it_can_change_the_setting_field_name_prefix()
    {
        add_filter('acf/banner_manager/field_prefix', function() { return 'custom-thingy'; });

        $this->assertEquals('custom-thingy-test', acf_banner_manager_field_name('test'));

        remove_filter('acf/banner_manager/field_prefix', function() { return 'custom-thingy'; });
    }

    /** @test */
    public function it_will_retreive_proper_setting_value()
    {
        $field = $this->acfLocalField('text', 'acf-banner_manager-test_field');
        update_field('acf-banner_manager-test_field', 'my value', 'options');

        $this->assertEquals('my value', acf_banner_manager_get_value('test_field'));
    }

    /** @test */
    public function it_can_filter_setting_value_globally()
    {
        $field = $this->acfLocalField('text', 'acf-banner_manager-test_field');
        update_field('acf-banner_manager-test_field', 'my value', 'options');

        add_filter('acf/banner_manager/setting_value', function() { return 'updated!'; });

        $this->assertEquals('updated!', acf_banner_manager_get_value('test_field'));
    }

    /** @test */
    public function it_can_filter_setting_value_by_name()
    {
        $field = $this->acfLocalField('text', 'acf-banner_manager-test_field');
        update_field('acf-banner_manager-test_field', 'my value', 'options');

        $field = $this->acfLocalField('text', 'acf-banner_manager-test_field_2');
        update_field('acf-banner_manager-test_field_2', 'my value 2', 'options');

        add_filter('acf/banner_manager/setting_value/name=test_field', function() { return 'updated!'; });

        $this->assertEquals('updated!', acf_banner_manager_get_value('test_field'));
        $this->assertEquals('my value 2', acf_banner_manager_get_value('test_field_2'));
    }

    /** @test */
    public function it_will_set_proper_setting_value()
    {
        $field = $this->acfLocalField('text', 'acf-banner_manager-test_field');

        acf_banner_manager_save_value('test_field', 'my value');

        $this->assertEquals('my value', get_field('acf-banner_manager-test_field', 'options'));
    }

    /** @test */
    public function it_will_grab_globally_managed_post_types()
    {
        acf_banner_manager_save_value('global_post_types', ['post']);

        $this->assertEquals(['post'], acf_banner_manager_global_post_types());
    }

    /** @test */
    public function it_can_filter_globally_managed_post_types()
    {
        acf_banner_manager_save_value('global_post_types', ['post']);
        add_filter('acf/banner_manager/global_post_types', function($postTypes) {
            return ['page'];
        });

        $this->assertEquals(['page'], acf_banner_manager_global_post_types());
    }

    /** @test */
    public function it_will_grab_globally_managed_taxonomies()
    {
        acf_banner_manager_save_value('global_taxonomies', ['post_tag']);

        $this->assertEquals(['post_tag'], acf_banner_manager_global_taxonomies());
    }

    /** @test */
    public function it_can_filter_globally_managed_taxonomies()
    {
        acf_banner_manager_save_value('global_taxonomies', ['post_tag']);
        add_filter('acf/banner_manager/global_taxonomies', function($postTypes) {
            return ['category'];
        });

        $this->assertEquals(['category'], acf_banner_manager_global_taxonomies());
    }

    /** @test */
    public function it_will_grab_excluded_page_templates()
    {
        acf_banner_manager_save_value('excluded_templates', ['asdf', 'qewr']);

        $this->assertEquals(['asdf', 'qewr'], acf_banner_manager_excluded_templates());
    }

    /** @test */
    public function it_can_filter_disabled_page_templates()
    {
        $page = $this->factory->post->create_and_get(['post_type' => 'page']);
        acf_banner_manager_save_value('excluded_templates', [$page->ID]);
        add_filter('acf/banner_manager/excluded_templates', '__return_empty_array');

        $this->assertEmpty(acf_banner_manager_excluded_templates());
    }

    /** @test */
    public function it_can_add_merge_tags()
    {
        acf_banner_manager_add_terge_tag('foo', 'bar', 'get_the_title');

        $this->assertArrayHasKey('{{foo}}', acf_banner_manager_merge_tags());
    }
}
