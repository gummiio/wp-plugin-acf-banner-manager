<?php

namespace Tests;

use Tests\AcfSupports;

class TestCase extends \WP_UnitTestCase
{
    use AcfSupports;

    public function setUp()
    {
        parent::setUp();

        $this->acfClearLocals();
        acf_disable_cache();
    }

    protected function getOutput($callable, $args = [])
    {
        ob_start();
        call_user_func_array($callable, $args);
        return ob_get_clean();
    }

    protected function onOptionPage($custom = null)
    {
        acf_banner_manager('optionPage')->getOptionGroup()->loadOptionFieldGroup();

        if (count(acf_options_page()->get_pages()) > 1) {
            $screen = 'options_page_' . ($custom? : acf_banner_manager_option_page('menu_slug'));
        } else {
            $screen = 'toplevel_page_' . ($custom? : acf_banner_manager_option_page('menu_slug'));
        }

        set_current_screen($screen);
        do_action("load-{$screen}");
    }

    protected function debugOptions()
    {
        global $wpdb;

        dump($wpdb->get_results("
            SELECT option_name, option_value
            FROM $wpdb->options
            WHERE option_name LIKE '%acf-banner_manager%'
        "));
    }
}
