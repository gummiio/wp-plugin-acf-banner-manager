<?php
$pluginDir = dirname(dirname(__FILE__));
$wpRoot    = dirname(dirname(dirname(dirname($pluginDir))));
$testRoot  = "$wpRoot/tests/phpunit";

require_once $testRoot . '/includes/functions.php';

tests_add_filter('muplugins_loaded', function() {
    update_option('active_plugins', [
        'advanced-custom-fields-pro/acf.php',
        'advanced-custom-fields-banner_manager/index.php'
    ]);
});

require_once $testRoot . '/includes/bootstrap.php';
