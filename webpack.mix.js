let mix = require('laravel-mix');

mix.setPublicPath("./");
mix.sass('assets/scss/acf-banner-manager.scss', 'assets/css/')
   .js('assets/js/acf-banner-manager.js', 'assets/js/acf-banner-manager.min.js')
   .options({processCssUrls: false});
